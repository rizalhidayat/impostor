//
//  DataLoader.swift
//  Locla
//
//  Created by Rizal Hidayat on 10/08/20.
//  Copyright © 2020 Apple Developer Academy. All rights reserved.
//

import Foundation

class DataLoader {
    private static let fileExtension = "json"
    private static let locationFilename = "Location"
    
    private static let topicFilename = "Topic"
    private static let subtopicFilename = "Subtopic"
    private static let chatChallengeFilename = "ChatChallenge"
    private static let listeningChallengeFilename = "ListeningChallenge"
    private static let vocabularyChallengeFilename = "VocabularyChallenge"
    private static let vocabularyFilename = "Vocabulary"
    private static let vocabularyCategoryFilename = "VocabularyCategory"
    private static let randomTipFilename = "RandomTip"
    private static let rewardFilename = "Reward"
    private static let rewardCategoryFilename = "RewardCategory"
    
    static func getCurrentLocation() -> Int {
        let defaults = UserDefaults.standard
        return defaults.integer(forKey: "Location") != 0 ? defaults.integer(forKey: "Location") : 1
    }
    
    static func getCurrentLocationName() -> String {
        let name = getAllLocation().filter{$0.id == getCurrentLocation()}.first?.name
        return name ?? "Makassar"
    }
    
    static func setCurrentLocation(locationId: Int){
        let defaults = UserDefaults.standard
        defaults.set(locationId, forKey: "Location")
    }
    
    static func getAllLocation() -> [Location] {
        var locations = [Location]()
        if let path = Bundle.main.url(forResource: locationFilename, withExtension: fileExtension) {
            do {
                let data = try Data(contentsOf: path)
                let decoder = JSONDecoder()
                locations = try decoder.decode([Location].self, from: data)
            } catch {
                print(error)
            }
        }
        return locations
    }
    
    static func getTopics(locationID: Int) -> [Topic] {
        var topics = [Topic]()
        if let path = Bundle.main.url(forResource: topicFilename + "-\(getCurrentLocationName())", withExtension: fileExtension) {
            do {
                let data = try Data(contentsOf: path)
                let decoder = JSONDecoder()
                topics = try decoder.decode([Topic].self, from: data)
            } catch {
                print(error)
            }
        }
        return topics
    }
    
    static func getSubtopics(topicID: Int) -> [Subtopic] {
        var subtopics : [Subtopic] = []
        let fm = FileManager.default
        do {
            let documentDirectory = try fm.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
            let subUrl = documentDirectory.appendingPathComponent("\(subtopicFilename)-\(getCurrentLocationName()).\(fileExtension)")
            var path : URL
            if fm.fileExists(atPath: subUrl.path) {
                path = subUrl
            } else {
                path = Bundle.main.url(forResource: subtopicFilename + "-\(getCurrentLocationName())", withExtension: fileExtension)!
            }
            let data = try Data(contentsOf: path)
            let decoder = JSONDecoder()
            subtopics = try decoder.decode([Subtopic].self, from: data)
            subtopics = subtopics.filter{ $0.topicID == topicID}
        } catch {
            print(error)
        }
        return subtopics
    }
    
    static func getSubtopic(subtopicID: Int) -> Subtopic {
        var subtopics = [Subtopic]()
        if let path = Bundle.main.url(forResource: subtopicFilename + "-\(getCurrentLocationName())", withExtension: fileExtension) {
            do {
                let data = try Data(contentsOf: path)
                let decoder = JSONDecoder()
                subtopics = try decoder.decode([Subtopic].self, from: data)
                subtopics = subtopics.filter{ $0.id == subtopicID}
            } catch {
                print(error)
            }
        }
        return subtopics[0]
    }
    
    static func getChatChallenges(subtopicID: Int) -> [ChatChallenge] {
        var chatChallenges = [ChatChallenge]()
        if let path = Bundle.main.url(forResource: chatChallengeFilename + "-\(getCurrentLocationName())", withExtension: fileExtension) {
            do {
                let data = try Data(contentsOf: path)
                let decoder = JSONDecoder()
                chatChallenges = try decoder.decode([ChatChallenge].self, from: data)
                chatChallenges = chatChallenges.filter{ $0.subtopicID == subtopicID}
            } catch {
                print(error)
            }
        }
        return chatChallenges
    }
    
    static func getListeningChallenges(subtopicID: Int) -> [ListeningChallenge] {
        var listeningChallenges = [ListeningChallenge]()
        if let path = Bundle.main.url(forResource: listeningChallengeFilename + "-\(getCurrentLocationName())", withExtension: fileExtension) {
            do {
                let data = try Data(contentsOf: path)
                let decoder = JSONDecoder()
                listeningChallenges = try decoder.decode([ListeningChallenge].self, from: data)
                listeningChallenges = listeningChallenges.filter{ $0.subtopicID == subtopicID}
            } catch {
                print(error)
            }
        }
        return listeningChallenges
    }
    
    static func getVocabularyChallenges(subtopicID: Int) -> [VocabularyChallenge] {
        var vocabularyChallenges = [VocabularyChallenge]()
        if let path = Bundle.main.url(forResource: vocabularyChallengeFilename + "-\(getCurrentLocationName())", withExtension: fileExtension) {
            do {
                let data = try Data(contentsOf: path)
                let decoder = JSONDecoder()
                vocabularyChallenges = try decoder.decode([VocabularyChallenge].self, from: data)
                vocabularyChallenges = vocabularyChallenges.filter{ $0.subtopicID == subtopicID}
            } catch {
                print(error)
            }
        }
        return vocabularyChallenges
    }
    
    static func getVocabularyCategories(locationID: Int) -> [VocabularyCategory] {
        var vocabularyCategories = [VocabularyCategory]()
        if let path = Bundle.main.url(forResource: vocabularyCategoryFilename + "-\(getCurrentLocationName())", withExtension: fileExtension) {
            do {
                let data = try Data(contentsOf: path)
                let decoder = JSONDecoder()
                vocabularyCategories = try decoder.decode([VocabularyCategory].self, from: data)
            } catch {
                print(error)
            }
        }
        return vocabularyCategories
    }
    
    static func getVocabularies(subtopicID: Int) -> [Vocabulary] {
        var vocabularies = [Vocabulary]()
        if let path = Bundle.main.url(forResource: vocabularyFilename + "-\(getCurrentLocationName())", withExtension: fileExtension) {
            do {
                let data = try Data(contentsOf: path)
                let decoder = JSONDecoder()
                vocabularies = try decoder.decode([Vocabulary].self, from: data)
                vocabularies = vocabularies.filter{ $0.subtopicID == subtopicID}
            } catch {
                print(error)
            }
        }
        return vocabularies
    }
    
    static func getVocabularies(categoryID: Int) -> [Vocabulary] {
        var vocabularies = [Vocabulary]()
        if let path = Bundle.main.url(forResource: vocabularyFilename + "-\(getCurrentLocationName())", withExtension: fileExtension) {
            do {
                let data = try Data(contentsOf: path)
                let decoder = JSONDecoder()
                vocabularies = try decoder.decode([Vocabulary].self, from: data)
                vocabularies = vocabularies.filter{ $0.categoryID == categoryID}
            } catch {
                print(error)
            }
        }
        return vocabularies
    }
    
    static func getRandomTip(locationID: Int) -> String {
        var tip = ""
        if let path = Bundle.main.url(forResource: randomTipFilename + "-\(getCurrentLocationName())", withExtension: fileExtension){
            do {
                let data = try Data(contentsOf: path)
                let decoder = JSONDecoder()
                let tips = try decoder.decode([RandomTip].self, from: data)
                tip = tips[Int.random(in: 0..<tips.count)].tips ?? ""
            } catch {
                print(error)
            }
        }
        return tip
    }
    
    static func getRewards(locationID: Int, categoryID: Int) -> [Reward] {
        var rewards = [Reward]()
        if let path = Bundle.main.url(forResource: rewardFilename + "-\(getCurrentLocationName())", withExtension: fileExtension){
            do {
                let data = try Data(contentsOf: path)
                let decoder = JSONDecoder()
                rewards = try decoder.decode([Reward].self, from: data)
                rewards = rewards.filter{ $0.categoryID == categoryID}
            } catch {
                print(error)
            }
        }
        return rewards
    }
    
    static func getRewardCategories() -> [RewardCategory] {
        var rewardCategories = [RewardCategory]()
        if let path = Bundle.main.url(forResource: rewardCategoryFilename + "-\(getCurrentLocationName())", withExtension: fileExtension){
            do {
                let data = try Data(contentsOf: path)
                let decoder = JSONDecoder()
                rewardCategories = try decoder.decode([RewardCategory].self, from: data)
            } catch {
                print(error)
            }
        }
        return rewardCategories
    }
    
    static func updateSubtopic(subtopicId: Int, star: Int, firstTry: Int){
        var subtopics : [Subtopic] = []
        let fm = FileManager.default
        do {
            let documentDirectory = try fm.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
            let subUrl = documentDirectory.appendingPathComponent("\(subtopicFilename)-\(getCurrentLocationName()).\(fileExtension)")
            var path : URL
            if fm.fileExists(atPath: subUrl.path) {
                path = subUrl
            } else {
                path = Bundle.main.url(forResource: subtopicFilename + "-\(getCurrentLocationName())", withExtension: fileExtension)!
            }
            let data = try Data(contentsOf: path)
            let decoder = JSONDecoder()
            subtopics = try decoder.decode([Subtopic].self, from: data)
            
            var subtopic : Subtopic?
            var index = 0
            var changedIndex = 0
            for sub in subtopics{
                if sub.id == subtopicId {
                    subtopic = sub
                    if subtopic?.starGained ?? 0 < star {
                        subtopic?.starGained = star
                    }
                    subtopic?.status = Status.cleared
                    changedIndex = index
                }
                index += 1
            }
            subtopics.remove(at: changedIndex)
            subtopics.insert(subtopic!, at: changedIndex)
            
            if subtopics.indices.contains(changedIndex + 1) {
                subtopics[changedIndex + 1].status = Status.unlocked
            }
            let encoder = JSONEncoder()
            encoder.outputFormatting = .prettyPrinted
            let JsonData = try encoder.encode(subtopics)
            try JsonData.write(to: subUrl)
        } catch {
            print(error)
        }
    }
    
   
    
    static func getUsername() -> String {
        let defaults = UserDefaults.standard
        return defaults.string(forKey: "User") ?? "User"
    }
    
    static func setUsername(name: String) {
        let defaults = UserDefaults.standard
        defaults.set(name, forKey: "User")
    }
}
