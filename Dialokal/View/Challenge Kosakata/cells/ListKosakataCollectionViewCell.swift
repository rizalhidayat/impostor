//
//  ListKosakataCollectionViewCell.swift
//  Dialokal
//
//  Created by Tony Varian Yoditanto on 24/09/20.
//  Copyright © 2020 Apple Developer Academy. All rights reserved.
//

import UIKit

protocol ListKosakataCollectionViewCellDelegate {
    func clickedSoundButton(for index : Int)
}

class ListKosakataCollectionViewCell: UICollectionViewCell {

    static let cellID = "ListKosakataCollectionViewCell"
    @IBOutlet weak var kosakataLabel: UILabel!
    @IBOutlet weak var terjemahanLabel: UILabel!
    @IBOutlet weak var speakerButton: UIButton!
    var indexPath : IndexPath?
    var delegate : ListKosakataCollectionViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        configureCell()
    }

    func configureCell(){
        self.layer.cornerRadius = 10.0
        self.kosakataLabel.text = "Aku = Aku"
        self.speakerButton.layer.cornerRadius = 18.0
        self.speakerButton.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.speakerButton.layer.borderWidth = 1.0
    }
    
    @IBAction func didTapSoundButton(_ sender: Any) {
        delegate?.clickedSoundButton(for : indexPath?.row ?? 0)
    }
}
