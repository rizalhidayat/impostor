//
//  vocabularyCategoryCell.swift
//  Locla
//
//  Created by Bagus setiawan on 08/08/20.
//  Copyright © 2020 Apple Developer Academy. All rights reserved.
//

import UIKit

class VocabularyCategoryCell: UICollectionViewCell {
    
    @IBOutlet weak var backgroundCell: UIView!
    @IBOutlet weak var categoryVocalImage: UIImageView!
    @IBOutlet weak var categoryVocalLabel: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        self.layer.cornerRadius = 20.0
        self.layer.borderWidth = 2.0
        self.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.categoryVocalImage.layer.cornerRadius = 50.0
        //self.backgroundCell.layer.cornerRadius = 21.0
        // Initialization code
    }

}
