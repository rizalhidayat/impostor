//
//  LocationCollectionViewCell.swift
//  Dialokal
//
//  Created by Tony Varian Yoditanto on 24/09/20.
//  Copyright © 2020 Apple Developer Academy. All rights reserved.
//

import UIKit

class LocationCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var lokasiLabel: UILabel!
    @IBOutlet weak var lokasiImageView: UIImageView!
    @IBOutlet weak var downloadImageView: UIImageView!
    static let cellID = "LocationCollectionViewCell"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        configureCell()
        
        // Initialization code
    }
    
    func configureCell(){
        //cell configureation
        self.layer.cornerRadius = 20.0
        self.layer.borderWidth = 1.0
//        self.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 1.0).cgColor
//        self.layer.shadowOffset = CGSize(width: 0.0, height: 4.0)
//        self.layer.shadowOpacity = 0.5
//        self.layer.shadowRadius = 2.0
//        self.layer.masksToBounds = false
        
        //image configuration
        self.lokasiImageView.image = UIImage(named: "star")
        self.lokasiImageView.layer.cornerRadius = 25.0
        self.lokasiImageView.layer.borderWidth = 2.0
        self.downloadImageView.image = UIImage(named: "star")
        self.downloadImageView.layer.cornerRadius = 25.0
        
        //label configuration
        //self.lokasiLabel.adjustsFontSizeToFitWidth = true
//        self.lokasiLabel.minimumScaleFactor = 0.5;
//        self.lokasiLabel.adjustsFontSizeToFitWidth = true;
    }

}
