//
//  SubtopikTableViewCell.swift
//  Dialokal
//
//  Created by Tony Varian Yoditanto on 14/10/20.
//  Copyright © 2020 Apple Developer Academy. All rights reserved.
//

import UIKit

class SubtopikTableViewCell: UITableViewCell {

    static let cellID = "SubtopikTableViewCell"
    @IBOutlet weak var subtopikImage: UIImageView!
    @IBOutlet weak var subtopikLabel: UILabel!
    @IBOutlet weak var challengeLabel: UILabel!
    @IBOutlet weak var starImage: UIImageView!
    @IBOutlet weak var totalStar: UILabel!
    @IBOutlet weak var circleView: UIView!
    @IBOutlet weak var challengeView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        configureUI()
    }
    
    func configureUI(){
        self.circleView.layer.cornerRadius = 55.0
        self.circleView.layer.borderWidth = 4.0
        self.circleView.layer.borderColor = #colorLiteral(red: 0.3294117647, green: 0.3294117647, blue: 0.337254902, alpha: 1)
        self.subtopikImage.layer.cornerRadius = 47.0
        self.subtopikImage.layer.borderWidth = 2.0
        self.subtopikImage.layer.borderColor = #colorLiteral(red: 0.3294117647, green: 0.3294117647, blue: 0.337254902, alpha: 1)
        self.challengeView.layer.cornerRadius = 11.0
        self.challengeView.layer.borderWidth = 1.0
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
