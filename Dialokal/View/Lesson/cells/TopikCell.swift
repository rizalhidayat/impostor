//
//  TopikCell.swift
//  Dialokal
//
//  Created by Tony Varian Yoditanto on 13/10/20.
//  Copyright © 2020 Apple Developer Academy. All rights reserved.
//

import UIKit

protocol TopikCellDelegate {
    func toggleSection(_ header: TopikCell, section: Int)
}

class TopikCell: UITableViewHeaderFooterView {


    static let cellID = "TopikCell"
    @IBOutlet weak var topikLabel: UILabel!
    @IBOutlet weak var totalSubtopikLabel: UILabel!
    @IBOutlet weak var totalStarLabel: UILabel!
    @IBOutlet weak var topikImage: UIImageView!
    @IBOutlet weak var topikView: UIView!
    @IBOutlet weak var totalSubtopikView: UIView!
    
    var delegate: TopikCellDelegate?
    var section: Int = 0
       
    override func awakeFromNib() {
        super.awakeFromNib()
        let recognizer = UITapGestureRecognizer(target: self, action: #selector(tapHeader(_:)))
        // Add gesture recognizer to the view
        self.addGestureRecognizer(recognizer)

    }
       
    @objc func tapHeader(_ gestureRecognizer: UITapGestureRecognizer) {
        guard let cell = gestureRecognizer.view as? TopikCell else {
            return
        }
           
        delegate?.toggleSection(self, section: cell.section)
    }
}
