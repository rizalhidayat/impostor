//
//  NewWordCollectionView.swift
//  Locla
//
//  Created by Bagus setiawan on 18/08/20.
//  Copyright © 2020 Apple Developer Academy. All rights reserved.
//

import UIKit

class NewWordCollectionView: UICollectionViewCell {
    
    @IBOutlet weak var newWordLabel: UILabel!
    
    static let cellID = "NewWordCollectionView"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        configureBackground()
    }
    
    func configureBackground(){
        self.layer.cornerRadius = 5.0
    }

}
