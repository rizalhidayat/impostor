//
//  RightMessage.swift
//  Locla
//
//  Created by Bagus setiawan on 15/08/20.
//  Copyright © 2020 Apple Developer Academy. All rights reserved.
//

import UIKit

class RightMessage: UITableViewCell {
    
    @IBOutlet weak var backgroundRightMessageView : UIView!
    @IBOutlet weak var messageRightLabel : UILabel!
    @IBOutlet weak var messageRightSecondaryLabrl : UILabel!
    
    static let cellID = "RightMessage"

    override func awakeFromNib() {
        super.awakeFromNib()
        
        //configureBackground()
        backgroundRightMessageView.backgroundColor = .white
        
        backgroundRightMessageView.backgroundColor = #colorLiteral(red: 0.968627451, green: 0.7843137255, blue: 0.262745098, alpha: 1)
        backgroundRightMessageView.layer.cornerRadius = 21.0
        backgroundRightMessageView.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        backgroundRightMessageView.layer.borderWidth = 1.0
        backgroundColor = .clear
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureBackground(){
        self.backgroundRightMessageView.layer.cornerRadius = 10
        }
    
//    func configureCell(message: ChatChallenge) {
//        messageRightLabel.text = message.opponentChatText
//       }
    
}
