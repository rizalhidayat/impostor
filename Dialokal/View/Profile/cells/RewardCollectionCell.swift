//
//  rewardCollectionCell.swift
//  Dialokal
//
//  Created by Tony Varian Yoditanto on 06/11/20.
//  Copyright © 2020 Apple Developer Academy. All rights reserved.
//

import UIKit

class RewardCollectionCell: UICollectionViewCell {

    @IBOutlet weak var rewardImage: UIImageView!
    @IBOutlet weak var rewardCategoryLbl: UILabel!
    @IBOutlet weak var starLbl: UILabel!
    static let cellID = "RewardCollectionCell"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        configureUI()
    }
    
    func configureUI(){
        rewardImage.layer.cornerRadius = 22.5
        self.layer.cornerRadius = 20.0
        self.layer.borderWidth = 1.0
        self.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    }
    

}
