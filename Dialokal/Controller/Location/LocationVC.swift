//
//  LocationViewController.swift
//  Locla
//
//  Created by Edward da Costa on 17/08/20.
//  Copyright © 2020 Apple Developer Academy. All rights reserved.
//

import UIKit

class LocationVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource{
    var locationId : Int?
    var location : [Location] = []
    @IBOutlet weak var collectionView: UICollectionView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fetchLocation()
        registerCollectionView()
    }
    
    func fetchLocation (){
         location = DataLoader.getAllLocation()
    }
    
    func registerCollectionView(){
        let nib = UINib(nibName: LocationCollectionViewCell.cellID, bundle: Bundle.main)
        self.collectionView.register(nib, forCellWithReuseIdentifier: LocationCollectionViewCell.cellID)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return location.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "LocationCollectionViewCell", for: indexPath) as! LocationCollectionViewCell
        cell.lokasiLabel.text = location[indexPath.row].name
        cell.lokasiImageView.image = UIImage(named: location[indexPath.row].imageFilename ?? "")
        return cell
    }
  
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: false)
        DataLoader.setCurrentLocation(locationId: self.location[indexPath.row].id)
        performSegue(withIdentifier: "LocationToTopic", sender: indexPath)
    }
    
    @IBAction func onCloseButtonClicked(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
}
