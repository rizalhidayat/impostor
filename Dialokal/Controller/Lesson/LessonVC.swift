//
//  LessonVC.swift
//  Locla
//
//  Created by Rizal Hidayat on 04/08/20.
//  Copyright © 2020 Apple Developer Academy. All rights reserved.
//

import UIKit

class LessonVC: UIViewController{
    var locationId : Int?
    var topics : [Topic] = []
    var location : [Location] = []
    var collapsed : [Bool] = []
    @IBOutlet weak var locationBtn: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var locationView: UIView!
    @IBOutlet weak var locationImageView: UIImageView!
    @IBOutlet weak var navbarLessonView: UIView!
    
    override func viewDidLoad() {
    //   super.viewDidLoad()
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        fetchTopics()
        configureUI()
        setupView()
        tableView.reloadData()
    }
    
    func configureUI(){
        self.locationView.layer.cornerRadius = 18.0
        self.locationView.layer.borderWidth = 2.0
        self.locationImageView.layer.cornerRadius = 30.0
        self.locationImageView.layer.borderWidth = 2.0
        self.locationImageView.image = UIImage(named: DataLoader.getCurrentLocationName() + "_Circle")
        self.locationBtn.titleLabel?.minimumScaleFactor = 0.5;
        self.locationBtn.titleLabel?.adjustsFontSizeToFitWidth = true;
        addCustomBorders()
    }
    
    func addCustomBorders() {
        let thickness: CGFloat = 1.0
        
        let bottomBorder = CALayer()
        
        bottomBorder.frame = CGRect(x:0, y: self.navbarLessonView.frame.size.height - thickness, width: self.navbarLessonView.frame.size.width, height: thickness)
        bottomBorder.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)

        self.navbarLessonView.layer.addSublayer(bottomBorder)

    }
    
    func fetchTopics(){
        locationId = DataLoader.getCurrentLocation()
        topics = DataLoader.getTopics(locationID: locationId ?? 1)
        
        for _ in 0 ... topics.count-1{
            self.collapsed.append(true)
        }
    }

    func setupView() {
        let locations = DataLoader.getAllLocation()
        let locationName = locations.filter {$0.id == locationId}.first?.name
        locationBtn.setTitle(locationName, for: .normal)
        //self.locationImageView.image = UIImage(named: lo)
        registerTopikCell()
        registerSubtopikCell()
    }

    func registerTopikCell(){
        let nib = UINib(nibName: TopikCell.cellID, bundle: Bundle.main)
        self.tableView.register(nib, forHeaderFooterViewReuseIdentifier: TopikCell.cellID)
    }
    
    func registerSubtopikCell(){
        let nib = UINib(nibName: SubtopikTableViewCell.cellID, bundle: Bundle.main)
        self.tableView.register(nib, forCellReuseIdentifier: SubtopikTableViewCell.cellID)
    }
    
    @IBAction func unwindToTopic(_ unwindSegue: UIStoryboardSegue) {
        viewWillAppear(true)
        // Use data from the view controller which initiated the unwind segue
    }
}

extension LessonVC : UITableViewDataSource ,UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return topics.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let subtopikPerSec = DataLoader.getSubtopics(topicID: topics[section].id)
        return collapsed[section] ? 0 : subtopikPerSec.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: SubtopikTableViewCell.cellID, for: indexPath) as? SubtopikTableViewCell else { return UITableViewCell() }
        var subtopics : [Subtopic] = []
        subtopics = DataLoader.getSubtopics(topicID: topics[indexPath.section].id)
        
        //Configure image
        if subtopics[indexPath.row].status == Status.locked {
            cell.subtopikImage.image = UIImage(named: "PadlockGray_Circle")
            cell.challengeView.backgroundColor = UIColor(named: "InactiveYellow")
         } else {
            cell.subtopikImage.image = UIImage(named: subtopics[indexPath.row].imageFilename ?? "star")
            cell.challengeView.backgroundColor = UIColor(named: "PrimaryYellow")
        }
        //Configure label
        cell.subtopikLabel.text = subtopics[indexPath.row].name
        cell.challengeLabel.text = subtopics[indexPath.row].challengeType?.rawValue
        cell.totalStar.text = "\(subtopics[indexPath.row].starGained) / \(subtopics[indexPath.row].totalStar)"

        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: TopikCell.cellID) as! TopikCell
        
        //Configure Label
        header.topikLabel.text = topics[section].name
        let subtopics = DataLoader.getSubtopics(topicID: topics[section].id)
        header.totalSubtopikLabel.text = "\(subtopics.count) Topik"
        var stargained = 0
        var starMax = 0
        for subtopic in subtopics {
            stargained += subtopic.starGained
            starMax += subtopic.totalStar
        }
        header.totalStarLabel.text = "\(stargained) / \(starMax)"
        
        //Configure Image
        header.topikImage.image = UIImage(named: topics[section].imageFilename ?? "Person1")
        header.topikImage.layer.cornerRadius = 34.0
        header.topikImage.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        header.topikImage.layer.borderWidth = 2.0
        
        //Configure topikView
        header.topikView.layer.borderWidth = 2.0
        header.topikView.layer.cornerRadius = 20.0
        
        //Configure totalSubtopikView
        header.totalSubtopikView.layer.cornerRadius = 11.0
        header.totalSubtopikView.layer.borderWidth = 1.0
        
        header.section = section
        header.delegate = self
        
        
        return header
    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 114.0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 136.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        var subtopics : [Subtopic] = []
        subtopics = DataLoader.getSubtopics(topicID: topics[indexPath.section].id)
        if subtopics[indexPath.row].status == Status.locked { return }
        
        if subtopics[indexPath.row].challengeType == ChallengeType.kosakata {
            performSegue(withIdentifier: "SubtopicToVocabChallenge", sender: indexPath)
        }
        if subtopics[indexPath.row].challengeType == ChallengeType.obrolan {
            performSegue(withIdentifier: "SubtopicToChatChallenge", sender: indexPath)
        }
        if subtopics[indexPath.row].challengeType == ChallengeType.mendengarkan {
            performSegue(withIdentifier: "SubtopicToListeningChallenge", sender: indexPath)
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "SubtopicToVocabChallenge" {
            guard let listKosakataVC = segue.destination as? ListKosakataVC,
                let indexPath = sender as? IndexPath
                else {
                    return
            }
            var subtopics : [Subtopic] = []
            subtopics = DataLoader.getSubtopics(topicID: topics[indexPath.section].id)
            listKosakataVC.subtopik = subtopics[indexPath.row]
            
        }
        
        if segue.identifier == "SubtopicToChatChallenge" {
            guard let introObrolanVC = segue.destination as? IntroObrolanVC,
                let indexPath = sender as? IndexPath
                else {
                    return
            }
            var subtopics : [Subtopic] = []
            subtopics = DataLoader.getSubtopics(topicID: topics[indexPath.section].id)
            introObrolanVC.subtopik = subtopics[indexPath.row]
        }
        
        if segue.identifier == "SubtopicToListeningChallenge" {
            guard let introListeningVC = segue.destination as? IntroListeningVC,
                let indexPath = sender as? IndexPath
                else {
                    return
            }
            var subtopics : [Subtopic] = []
            subtopics = DataLoader.getSubtopics(topicID: topics[indexPath.section].id)
            introListeningVC.subtopik = subtopics[indexPath.row]
        }
 
    }
}

extension LessonVC: TopikCellDelegate {
    
    func toggleSection(_ header: TopikCell, section: Int) {
        
        print("Collapsed")
        let collapsed = !self.collapsed[section]
        
        // Toggle collapse
        self.collapsed[section] = collapsed
        
        tableView.reloadSections(NSIndexSet(index: section) as IndexSet, with: .automatic)
        
    }
}

