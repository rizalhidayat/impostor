//
//  SubtopikVC.swift
//  Locla
//
//  Created by Tony Varian Yoditanto on 10/08/20.
//  Copyright © 2020 Apple Developer Academy. All rights reserved.
//

import UIKit

class SubtopikVC: UIViewController{
    
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var starLbl: UILabel!
    
    var topicId : Int?
    var subtopics : [Subtopic] = []
    
    var titleText, star : String?
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        titleLbl.text = titleText
        setStar()
        fetchSubtopiks()
        setupCollectionView()
        collectionView.reloadData()
    }
    
    func setStar(){
        let subtopics = DataLoader.getSubtopics(topicID: topicId ?? 1)
        var stargained = 0
        var starMax = 0
        for subtopic in subtopics {
            stargained += subtopic.starGained
            starMax += subtopic.totalStar
        }
        
        starLbl.text = "\(stargained) / \(starMax)"
    }
    
    func fetchSubtopiks(){
        subtopics = DataLoader.getSubtopics(topicID: topicId ?? 1)
    }
    
    func setupCollectionView() {
        registerCollectionView()
    }
    
    func registerCollectionView(){
        let nib = UINib(nibName: SubtopikCollectionViewCell.cellID, bundle: Bundle.main)
        self.collectionView.register(nib, forCellWithReuseIdentifier: SubtopikCollectionViewCell.cellID)
    }
    
}
// MARK: UICollectionViewDataSource
extension SubtopikVC: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return subtopics.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: SubtopikCollectionViewCell.cellID, for: indexPath) as? SubtopikCollectionViewCell else { return UICollectionViewCell() }
        cell.judulSubtopikLabel.text = subtopics[indexPath.row].name
        cell.jenisChallengeLabel.text = subtopics[indexPath.row].challengeType?.rawValue
        cell.raihanBintangLabel.text = "\(subtopics[indexPath.row].starGained) / \(subtopics[indexPath.row].totalStar)"
        cell.container.backgroundColor = subtopics[indexPath.row].status == Status.locked ? .systemGray : .systemOrange
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        let generator = UINotificationFeedbackGenerator()
        generator.notificationOccurred(.success)
        if subtopics[indexPath.row].status == Status.locked { return }
        
        if subtopics[indexPath.row].challengeType == ChallengeType.kosakata {
            performSegue(withIdentifier: "SubtopicToVocabChallenge", sender: indexPath.row)
        }
        if subtopics[indexPath.row].challengeType == ChallengeType.mendengarkan {
            performSegue(withIdentifier: "SubtopicToListeningChallenge", sender: indexPath.row)
        }
        if subtopics[indexPath.row].challengeType == ChallengeType.obrolan {
            performSegue(withIdentifier: "SubtopicToChatChallenge", sender: indexPath.row)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "SubtopicToVocabChallenge" {
            guard let listKosakataVC = segue.destination as? ListKosakataVC,
                let index = sender as? Int
                else {
                    return
            }
            listKosakataVC.subtopik = subtopics[index]
            
        }
        
        //Only for Test
        if segue.identifier == "SubtopicToListeningChallenge" {
            guard let introListeningVC = segue.destination as? IntroListeningVC,
                let index = sender as? Int
                else {
                    return
            }
            introListeningVC.subtopik = subtopics[index]
        }
        
        if segue.identifier == "SubtopicToChatChallenge" {
            guard let introObrolanVC = segue.destination as? IntroObrolanVC,
                let index = sender as? Int
                else {
                    return
            }
            introObrolanVC.subtopik = subtopics[index]
        }
        
    }
    
    @IBAction func unwindToSubtopic(_ unwindSegue: UIStoryboardSegue) {
        viewWillAppear(true)
        // Use data from the view controller which initiated the unwind segue
    }
    
    
    // MARK: - Sent Data to other ViewController
    //    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    //
    //    }
}
