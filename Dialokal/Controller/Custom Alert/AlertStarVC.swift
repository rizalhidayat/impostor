//
//  AlertStarVC.swift
//  Locla
//
//  Created by Tony Varian Yoditanto on 12/08/20.
//  Copyright © 2020 Apple Developer Academy. All rights reserved.
//

import UIKit
import AVFoundation

protocol AlertStarVCDelegate {
    func reload()
}

class AlertStarVC: UIViewController {

    @IBOutlet weak var keteranganLabel: UILabel!
   
    @IBOutlet weak var tipLabel: UILabel!
    @IBOutlet weak var lanjutButton: UIButton!
    @IBOutlet weak var customAlertView: UIView!
    @IBOutlet weak var starImageView: UIImageView!
    
    var starCount: Int = 3
    var keterangan: String = ""
    var tip: String = ""
    
    var parentID : Int = 0
    
    var delegateFromVocab : AlertStarVCDelegate?
    var delegateFromListen : AlertStarVCDelegate?
    var delegateFromChat : AlertStarVCDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureBackground()
        configureView()
        configureButton()
        // Do any additional setup after loading the view.
        playSound()
    }
    
    func playSound() {
        guard let correctURL = Bundle.main.url(forResource: "Correct", withExtension: "mp3") else { return }
        do {
            try player = AVAudioPlayer(contentsOf: correctURL)
        } catch {
            print(error)
        }
        player?.play()
    }
    func configureBackground(){
        self.customAlertView.layer.cornerRadius = 20.0
        self.customAlertView.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.customAlertView.layer.borderWidth = 2.0
    }
    
    func configureView(){
        switch starCount {
        case 3:
            keterangan = "Sempurna!"
            self.starImageView.image = UIImage(named: "Star3")
        case 2:
            keterangan = "Keren!"
            self.starImageView.image = UIImage(named: "Star2")
        case 1:
            keterangan = "Lumayan!"
            self.starImageView.image = UIImage(named: "Star1")
        default:
            print("AlertVC error")
        }
        self.keteranganLabel.text = keterangan
        self.tipLabel.text = tip
    }
    
    func configureButton(){
        self.lanjutButton.layer.cornerRadius = 26.5
        self.lanjutButton.layer.borderWidth = 1.0
        self.lanjutButton.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    }
    
    @IBAction func didTapLanjutButton(_ sender: Any) {
       
        if parentID == 1{
                   delegateFromVocab?.reload()
               }else
               if parentID == 2{
                   delegateFromChat?.reload()
               }else
               if parentID == 3{
                   delegateFromListen?.reload()
               }
               
               self.removeFromParent()
               self.view.removeFromSuperview()
               self.parent?.view.reloadInputViews()
    }

}
