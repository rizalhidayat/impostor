//
//  AlertRecapVC.swift
//  Dialokal
//
//  Created by Tony Varian Yoditanto on 24/10/20.
//  Copyright © 2020 Apple Developer Academy. All rights reserved.
//

import UIKit

protocol AlertRecapVCDelegate {
    func clickedRecapButton()
    func clickedResultButton()
}

class AlertRecapVC: UIViewController {

    @IBOutlet weak var recapButton: UIButton!
    @IBOutlet weak var recapButtonView: UIView!
    var delegate : AlertRecapVCDelegate?
    
    @IBOutlet weak var resultButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        configureButton()

        // Do any additional setup after loading the view.
    }
    
    func configureButton(){
        
        self.recapButtonView.layer.cornerRadius = 26.5
        self.recapButtonView.layer.borderWidth = 2.0
        self.recapButtonView.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        
        self.resultButton.layer.cornerRadius = 26.5
        self.resultButton.layer.borderWidth = 2.0
        self.resultButton.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    }
    
    @IBAction func didTapRecapButton(_ sender: Any) {
        delegate?.clickedRecapButton()
    }
    
    @IBAction func didTapResultButton(_ sender: Any) {
        delegate?.clickedResultButton()
    }
    

}
