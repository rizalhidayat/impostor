//
//  AlertExitVC.swift
//  Locla
//
//  Created by Tony Varian Yoditanto on 12/08/20.
//  Copyright © 2020 Apple Developer Academy. All rights reserved.
//

import UIKit

class AlertExitVC: UIViewController {

    @IBOutlet weak var alertImageview: UIImageView!
    @IBOutlet weak var lanjutButton: UIButton!
    @IBOutlet weak var keluarButton: UIButton!
    @IBOutlet weak var customAlertView: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureBackground()
        configureImageview()
        configureButton()
    }
    
    func configureBackground(){
        self.customAlertView.layer.cornerRadius = 20.0
        self.customAlertView.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.customAlertView.layer.borderWidth = 2.0
    }
    
    func configureImageview(){
        self.alertImageview.image = UIImage(named: "Exit")
    }
    
    func configureButton(){
        self.lanjutButton.layer.cornerRadius = 26.5
        self.lanjutButton.layer.borderWidth = 2.0
        self.lanjutButton.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        
        self.keluarButton.layer.cornerRadius = 26.5
        self.keluarButton.layer.borderWidth = 2.0
        self.keluarButton.layer.borderColor = #colorLiteral(red: 0.9639044404, green: 0.7331302762, blue: 0.3017231822, alpha: 1)
    }
    
    @IBAction func didTapLanjutButton(_ sender: Any) {
        self.removeFromParent()
        self.view.removeFromSuperview()
    }
    
    
    @IBAction func didTapKeluarButton(_ sender: Any) {
        self.view.window!.rootViewController?.dismiss(animated: true, completion: nil)
    }
    
}
