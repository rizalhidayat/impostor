//
//  ListKosakataVC.swift
//  Locla
//
//  Created by Tony Varian Yoditanto on 13/08/20.
//  Copyright © 2020 Apple Developer Academy. All rights reserved.
//

import UIKit
import AVFoundation

class ListKosakataVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var closeButton: UIButton!
    var subtopikId : Int?
    var subtopik : Subtopic?
    var vocabularies : [Vocabulary] = []
    var player : AVAudioPlayer?
    var petunjuk : String?
 
    @IBOutlet weak var mulaiButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureButton()
        configureCollectionView()
        fetchListKosakata()

    }
    
    func fetchListKosakata(){
        self.vocabularies = DataLoader.getVocabularies(subtopicID: subtopik?.id ?? 1)
        //self.vocabularies = DataLoader.getVocabularies(subtopicID: subtopikId ?? 1)
        //self.subtopik = DataLoader.getSubtopic(subtopicID: subtopikId)
    }
    
    func configureButton(){
        self.mulaiButton.layer.cornerRadius = 26.5
        self.mulaiButton.layer.borderWidth = 1.0
        self.mulaiButton.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.closeButton.layer.cornerRadius = 16.0
        self.closeButton.layer.borderWidth = 0.1
        self.closeButton.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    }

    func configureCollectionView(){
        registerCollectionView()
    }
    
    func registerCollectionView(){
        let nib = UINib(nibName: ListKosakataCollectionViewCell.cellID, bundle: Bundle.main)
        self.collectionView.register(nib, forCellWithReuseIdentifier: ListKosakataCollectionViewCell.cellID)
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return vocabularies.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ListKosakataCollectionViewCell.cellID, for: indexPath) as! ListKosakataCollectionViewCell
        cell.kosakataLabel.text = vocabularies[indexPath.row].meaning ?? ""
        cell.terjemahanLabel.text = vocabularies[indexPath.row].word ?? ""
        cell.indexPath = indexPath
        cell.delegate = self
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        playSound(for: indexPath.row)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ListToIntroChallengeVocab" {
            guard let introKosakataVC = segue.destination as? IntroKosakataVC
                else {
                    return
            }
            introKosakataVC.subtopik = subtopik
        }
    }
        
    @IBAction func didTapCloseButton(_ sender: Any) {
        let alertVC = UIStoryboard(name: "CustomAlert", bundle: nil).instantiateViewController(withIdentifier: "AlertExitVC") as! AlertExitVC
        
        alertVC.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        self.addChild(alertVC)
        self.view.addSubview(alertVC.view)
    }
    
    func playSound(for index: Int){
        Petunjuk.playPetunjuk(petunjukName: self.vocabularies[index].soundFilename ?? "")
    }
    
}

extension ListKosakataVC : ListKosakataCollectionViewCellDelegate{
    func clickedSoundButton(for index: Int) {
        playSound(for: index)
    }
}
