//
//  IntroKosakataVC.swift
//  Locla
//
//  Created by Tony Varian Yoditanto on 13/08/20.
//  Copyright © 2020 Apple Developer Academy. All rights reserved.
//

import UIKit

class IntroKosakataVC: UIViewController {
    
    var subtopik : Subtopic!

    @IBOutlet weak var judulSubtopikLabel: UILabel!
    @IBOutlet weak var subtopikImageview: UIImageView!
    @IBOutlet weak var deskripsiLabel: UILabel!
    @IBOutlet weak var misiLabel: UILabel!
    @IBOutlet weak var mulaiButton: UIButton!
    @IBOutlet weak var viewMisi: UIView!
    @IBOutlet weak var viewLbMisi: UIView!
    @IBOutlet weak var closeButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureButton()
        configureLabel()
        configureImage()
        configureView()
        // Do any additional setup after loading the view.
    }
    
    
    func configureButton(){
        self.closeButton.layer.cornerRadius = 16.0
        self.closeButton.layer.borderWidth = 0.1
        self.closeButton.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        
        self.mulaiButton.layer.cornerRadius = 26.5
        self.mulaiButton.layer.borderWidth = 1.0
        self.mulaiButton.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)

    }

    func configureLabel(){
        self.judulSubtopikLabel.text = subtopik.name ?? ""
        //self.deskripsiLabel.text = subtopik.mission ?? ""
        self.misiLabel.text = subtopik.subtopicDescription ?? ""
    }
    
    func configureImage(){
        self.subtopikImageview.image = UIImage(named: self.subtopik.imageFilename ?? "")
    }
    
    func addCustomBorders() {
        let thickness: CGFloat = 2.0
        
        let bottomBorder = CALayer()
        let leftBorder = CALayer()
        let rightBorder = CALayer()
        
        bottomBorder.frame = CGRect(x:0, y: self.viewLbMisi.frame.size.height - thickness, width: self.viewLbMisi.frame.size.width, height: thickness)
        bottomBorder.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        
        leftBorder.frame = CGRect(x: 0.0, y: 0.0, width: thickness, height: self.viewLbMisi.frame.size.height)
       leftBorder.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        
        rightBorder.frame = CGRect(x: self.viewLbMisi.frame.size.width-2.0, y: 0.0, width: thickness, height: self.viewLbMisi.frame.size.height)
       rightBorder.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        
        self.viewLbMisi.layer.addSublayer(bottomBorder)
        self.viewLbMisi.layer.addSublayer(leftBorder)
        self.viewLbMisi.layer.addSublayer(rightBorder)
    }
    
    func configureView(){
        self.viewMisi.layer.cornerRadius = 12.0
        self.viewMisi.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        self.viewMisi.layer.borderWidth = 2.0
        self.viewMisi.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        
        self.viewLbMisi.layer.cornerRadius = 12.0
        self.viewLbMisi.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
        self.viewLbMisi.layer.borderWidth = 2.0
        self.viewLbMisi.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "IntroToChallengeVocab" {
            guard let challengeKosakataVC = segue.destination as? ChallengeKosakataVC
                else {
                    return
            }
            challengeKosakataVC.subtopik = self.subtopik
        }
    }
    
    @IBAction func didTapCloseButton(_ sender: Any) {
        let alertVC = UIStoryboard(name: "CustomAlert", bundle: nil).instantiateViewController(withIdentifier: "AlertExitVC") as! AlertExitVC
        
        alertVC.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        self.addChild(alertVC)
        self.view.addSubview(alertVC.view)
    }
    
}
