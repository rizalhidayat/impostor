//
//  KosakataResultVC.swift
//  Locla
//
//  Created by Tony Varian Yoditanto on 13/08/20.
//  Copyright © 2020 Apple Developer Academy. All rights reserved.
//

import UIKit

class KosakataResultVC: UIViewController {

    var subtopik : Subtopic!
    var vocabularies : [Vocabulary] = []
    var starGained = 0
    var firstTry = 0
    var totalQuestion = 0
    @IBOutlet weak var subtopikImageview: UIImageView!
    @IBOutlet weak var keteranganIndoLabel: UILabel!
    @IBOutlet weak var keteranganLocalLabel: UILabel!
    @IBOutlet weak var star1Imageview: UIImageView!
    @IBOutlet weak var star2Imageview: UIImageView!
    @IBOutlet weak var star3Imageview: UIImageView!
    @IBOutlet weak var jempolImageview: UIImageView!
    @IBOutlet weak var percobaanPertamaLabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var randomTipLabel: UILabel!
    @IBOutlet weak var selesaiButton: UIButton!
    @IBOutlet weak var randomTipView: UIView!
    @IBOutlet weak var totalBintangImageview: UIImageView!
    @IBOutlet weak var totalBintangLabel: UILabel!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        saveStarAndFirstTry()
        configureImage()
        configureLabel()
        configureButton()
        fetchListKosakata()
        // Do any additional setup after loading the view.
    }
    
    func saveStarAndFirstTry(){
        DataLoader.updateSubtopic(subtopicId: self.subtopik.id, star: self.starGained, firstTry: firstTry)
        
    }
    
    func fetchListKosakata(){
        self.vocabularies = DataLoader.getVocabularies(subtopicID: subtopik?.id ?? 1)

    }
    
    func configureImage(){
        self.subtopikImageview.image = UIImage(named: "People1")
        self.star1Imageview.image = UIImage(named: "Star")
        self.star2Imageview.image = UIImage(named: "Star")
        self.star3Imageview.image = UIImage(named: "Star")
        self.jempolImageview.image = UIImage(named: "Image-1")
        self.totalBintangImageview.image = UIImage(named: "Star")
        self.star1Imageview.isHidden = true
        self.star2Imageview.isHidden = true
        self.star3Imageview.isHidden = true
    }
    
    func configureLabel(){
        self.keteranganIndoLabel.text = "Kamu yang terbaik!!"
        self.keteranganLocalLabel.text = "Kon Sangar"
        self.percobaanPertamaLabel.text = "\(self.firstTry)/\(self.totalQuestion)"
        self.randomTipView.layer.cornerRadius = 10.0
        self.totalBintangLabel.text = "\(self.starGained)/\(self.totalQuestion*3)"
        let locationID : Int = DataLoader.getCurrentLocation()
        self.randomTipLabel.text = DataLoader.getRandomTip(locationID: locationID)
    }
    
    func configureButton(){
        self.selesaiButton.layer.cornerRadius = 10.0
    }
    
    @IBAction func didTapSelesaiButton(_ sender: Any) {
        performSegue(withIdentifier: "ResultVocabToSubtopic", sender: sender)
    }
    
}
