//
//  ResultVC.swift
//  Dialokal
//
//  Created by Tony Varian Yoditanto on 23/09/20.
//  Copyright © 2020 Apple Developer Academy. All rights reserved.
//

import UIKit
import AVFoundation

class ResultVC: UIViewController {

    var subtopik : Subtopic!
    var vocabularies : [Vocabulary] = []
    var starGained = 0
    var firstTry = 0
    var totalQuestion = 0
    @IBOutlet weak var subtopikImageview: UIImageView!
    @IBOutlet weak var keteranganIndoLabel: UILabel!
//    @IBOutlet weak var keteranganLocalLabel: UILabel!
//    @IBOutlet weak var star1Imageview: UIImageView!
//    @IBOutlet weak var star2Imageview: UIImageView!
//    @IBOutlet weak var star3Imageview: UIImageView!
//    @IBOutlet weak var jempolImageview: UIImageView!
//    @IBOutlet weak var percobaanPertamaLabel: UILabel!
//    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var randomTipLabel: UILabel!
    @IBOutlet weak var selesaiButton: UIButton!
    @IBOutlet weak var randomTipHeaderView: UIView!
    @IBOutlet weak var randomTipView: UIView!
    @IBOutlet weak var totalBintangImageview: UIImageView!
    @IBOutlet weak var totalBintangLabel: UILabel!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        saveStarAndFirstTry()
        configureImage()
        configureLabel()
        configureButton()
        configureRandomTip()
        playSound()
        //loadParticles()
        //configureCollectionView()
        //fetchListKosakata()
        // Do any additional setup after loading the view.
    }
    
    func playSound() {
        guard let resultURL = Bundle.main.url(forResource: "Result", withExtension: "mp3") else { return }
        do {
            try player = AVAudioPlayer(contentsOf: resultURL)
        } catch {
            print(error)
        }
        player?.play()
    }
    
    func loadParticles(){
        let size = CGSize(width: 804.0, height: 1002.0)
        let host = UIView(frame: CGRect(x: 0.0, y: 0.0, width: size.width, height: size.height))
        self.view.addSubview(host)

        let particlesLayer = CAEmitterLayer()
        particlesLayer.frame = CGRect(x: 0.0, y: 0.0, width: size.width, height: size.height)

        host.layer.addSublayer(particlesLayer)
        host.layer.masksToBounds = true

        particlesLayer.backgroundColor = UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1.0).cgColor
        particlesLayer.emitterShape = .circle
        particlesLayer.emitterPosition = CGPoint(x: 296.0, y: 297.8)
        particlesLayer.emitterSize = CGSize(width: 1000.0, height: 900.0)
        particlesLayer.emitterMode = .surface
        particlesLayer.renderMode = .oldestLast



        let image1 = UIImage(named: "Square")?.cgImage

        let cell1 = CAEmitterCell()
        cell1.contents = image1
        cell1.name = "Square"
        cell1.birthRate = 22.0
        cell1.lifetime = 20.0
        cell1.velocity = 59.0
        cell1.velocityRange = -15.0
        cell1.xAcceleration = 5.0
        cell1.yAcceleration = 40.0
        cell1.emissionRange = 180.0 * (.pi / 180.0)
        cell1.spin = -28.6 * (.pi / 180.0)
        cell1.spinRange = 57.2 * (.pi / 180.0)
        cell1.scale = 0.01
        cell1.scaleRange = 0.3
        cell1.color = UIColor(red: 27.0/255.0, green: 54.0/255.0, blue: 250.0/255.0, alpha: 1.0).cgColor



        let image2 = UIImage(named: "Circle")?.cgImage

        let cell2 = CAEmitterCell()
        cell2.contents = image2
        cell2.name = "Circle"
        cell2.birthRate = 22.0
        cell2.lifetime = 20.0
        cell2.velocity = 59.0
        cell2.velocityRange = -15.0
        cell2.xAcceleration = 5.0
        cell2.yAcceleration = 40.0
        cell2.emissionRange = 180.0 * (.pi / 180.0)
        cell2.spin = -28.6 * (.pi / 180.0)
        cell2.spinRange = 57.2 * (.pi / 180.0)
        cell2.scale = 0.01
        cell2.scaleRange = 0.3
        cell2.color = UIColor(red: 75.0/255.0, green: 251.0/255.0, blue: 81.0/255.0, alpha: 1.0).cgColor



        let image3 = UIImage(named: "Spiral")?.cgImage

        let cell3 = CAEmitterCell()
        cell3.contents = image3
        cell3.name = "Spiral"
        cell3.birthRate = 22.0
        cell3.lifetime = 20.0
        cell3.velocity = 59.0
        cell3.velocityRange = -15.0
        cell3.xAcceleration = 5.0
        cell3.yAcceleration = 40.0
        cell3.emissionRange = 180.0 * (.pi / 180.0)
        cell3.spin = -28.6 * (.pi / 180.0)
        cell3.spinRange = 57.2 * (.pi / 180.0)
        cell3.scale = 0.01
        cell3.scaleRange = 0.3
        cell3.color = UIColor(red: 248.0/255.0, green: 78.0/255.0, blue: 249.0/255.0, alpha: 1.0).cgColor



        let image4 = UIImage(named: "Triangle")?.cgImage

        let cell4 = CAEmitterCell()
        cell4.contents = image4
        cell4.name = "Triangle"
        cell4.birthRate = 22.0
        cell4.lifetime = 20.0
        cell4.velocity = 59.0
        cell4.velocityRange = -15.0
        cell4.xAcceleration = 5.0
        cell4.yAcceleration = 40.0
        cell4.emissionRange = 180.0 * (.pi / 180.0)
        cell4.spin = -28.6 * (.pi / 180.0)
        cell4.spinRange = 57.2 * (.pi / 180.0)
        cell4.scale = 0.01
        cell4.scaleRange = 0.3
        cell4.color = UIColor(red: 236.0/255.0, green: 27.0/255.0, blue: 75.0/255.0, alpha: 1.0).cgColor


        let cell5 = CAEmitterCell()

        cell5.name = "Star"
        cell5.birthRate = 22.0
        cell5.lifetime = 20.0
        cell5.velocity = 59.0
        cell5.velocityRange = -15.0
        cell5.xAcceleration = 5.0
        cell5.yAcceleration = 40.0
        cell5.emissionRange = 180.0 * (.pi / 180.0)
        cell5.spin = -28.6 * (.pi / 180.0)
        cell5.spinRange = 57.2 * (.pi / 180.0)
        cell5.scale = 0.01
        cell5.scaleRange = 0.3
        cell5.color = UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 0.0/255.0, alpha: 1.0).cgColor

        particlesLayer.emitterCells = [cell1, cell2, cell3, cell4, cell5]
    }
    
    func saveStarAndFirstTry(){
        DataLoader.updateSubtopic(subtopicId: self.subtopik.id, star: self.starGained, firstTry: firstTry)

    }
    
//    func fetchListKosakata(){
//        self.vocabularies = DataLoader.getVocabularies(subtopicID: subtopik?.id ?? 1)
//
//    }
    
    func configureRandomTip(){
        let locationID : Int = DataLoader.getCurrentLocation()
        self.randomTipLabel.text = DataLoader.getRandomTip(locationID: locationID)
        
        self.randomTipHeaderView.layer.cornerRadius = 12.0
        self.randomTipHeaderView.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.randomTipHeaderView.layer.borderWidth = 2.0
        self.randomTipHeaderView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        
        self.randomTipView.layer.cornerRadius = 21.0
        self.randomTipView.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.randomTipView.layer.borderWidth = 2.0
        self.randomTipView.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
    }
    
    func configureImage(){
        self.subtopikImageview.image = UIImage(named: "Thumb")
//        self.star1Imageview.image = UIImage(named: "star")
//        self.star2Imageview.image = UIImage(named: "star")
//        self.star3Imageview.image = UIImage(named: "star")
//        self.jempolImageview.image = UIImage(named: "Image-1")
        self.totalBintangImageview.image = UIImage(named: "Star")
//        self.star1Imageview.isHidden = true
//        self.star2Imageview.isHidden = true
//        self.star3Imageview.isHidden = true
    }
    
    func configureLabel(){
        self.keteranganIndoLabel.text = "Kamu yang terbaik!"
//        self.keteranganLocalLabel.text = "Kon Sangar"
//        self.percobaanPertamaLabel.text = "\(self.firstTry)/\(self.totalQuestion)"
        self.totalBintangLabel.text = "\(self.starGained)/\(self.totalQuestion*3)"
    }
    
    func configureButton(){
        self.selesaiButton.layer.cornerRadius = 26.5
        self.selesaiButton.layer.borderWidth = 1.0
        self.selesaiButton.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    }
    
//    func configureCollectionView(){
//        registerCollectionView()
//    }
    
//    func registerCollectionView(){
//        let nib = UINib(nibName: NewWordsCollectionViewCell.cellID, bundle: Bundle.main)
////        self.collectionView.register(nib, forCellWithReuseIdentifier: NewWordsCollectionViewCell.cellID)
//    }
    
//    func numberOfSections(in collectionView: UICollectionView) -> Int {
//        return 1
//    }
//
//    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        return vocabularies.count
//    }
//
//    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: NewWordsCollectionViewCell.cellID, for: indexPath) as! NewWordsCollectionViewCell
//        cell.newWordLabel.text = vocabularies[indexPath.row].word
//        return cell
//    }
//
//    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        collectionView.deselectItem(at: indexPath, animated: true)
//        //performSegue(withIdentifier: "showResult", sender: indexPath)
//    }
    
    @IBAction func didTapSelesaiButton(_ sender: Any) {
        let generator = UINotificationFeedbackGenerator()
        generator.notificationOccurred(.success)
        performSegue(withIdentifier: "ResultVocabToSubtopic", sender: sender)
        self.view.window!.rootViewController?.reloadInputViews()
        self.view.window!.rootViewController?.dismiss(animated: true, completion: nil)
    }
    
}
