//
//  DetailRewardViewController.swift
//  Locla
//
//  Created by Rizal Hidayat on 17/08/20.
//  Copyright © 2020 Apple Developer Academy. All rights reserved.
//

import UIKit

class DetailRewardVC: UIViewController {

    @IBOutlet weak var picIV: UIImageView!
    @IBOutlet weak var sourceLbl: UILabel!
    @IBOutlet weak var descriptionLbl: UILabel!
    var reward : Reward?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
        // Do any additional setup after loading the view.
    }
    
    func setupView(){
        title = reward?.name
        picIV.image = UIImage(named: reward?.imageFilename ?? "")
        picIV.layer.cornerRadius = 20.0
        picIV.layer.borderWidth = 2.0
        picIV.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        sourceLbl.text = reward?.imageSource
        descriptionLbl.text = reward?.detail
    }
}
