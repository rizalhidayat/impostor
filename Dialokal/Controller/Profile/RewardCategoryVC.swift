//
//  RewardCategoryVC.swift
//  Dialokal
//
//  Created by Tony Varian Yoditanto on 06/11/20.
//  Copyright © 2020 Apple Developer Academy. All rights reserved.
//

import UIKit

class RewardCategoryVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    var starGained : Int?
    var rewardCategories : [RewardCategory] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureCollectionView()
        getRewardCategories()
        // Do any additional setup after loading the view.
    }
    
    func getRewardCategories(){
            rewardCategories = DataLoader.getRewardCategories()
        }
    
    func configureCollectionView(){
        registerCollectionView()
    }
    
    func registerCollectionView(){
        let nib = UINib(nibName: RewardCollectionCell.cellID, bundle: Bundle.main)
        self.collectionView.register(nib, forCellWithReuseIdentifier: RewardCollectionCell.cellID)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return rewardCategories.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "RewardCollectionCell", for: indexPath) as! RewardCollectionCell
                
        cell.rewardCategoryLbl.text = rewardCategories[indexPath.row].name
        cell.starLbl.text = "\(rewardCategories[indexPath.row].requiredStar)"
        cell.backgroundColor = (rewardCategories[indexPath.row].requiredStar <= starGained ?? 0) ? UIColor(named: "PrimaryYellow") : UIColor(named: "InactiveYellow")
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: false)
        if starGained ?? 0 > rewardCategories[indexPath.row].requiredStar {
            performSegue(withIdentifier: "RewardCategoryToReward", sender: indexPath.row)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
           if segue.identifier == "RewardCategoryToReward" {
               guard let rewardVC = segue.destination as? RewardVC,
                   let index = sender as? Int else { return }
               rewardVC.categoryId = rewardCategories[index].id
               rewardVC.title = rewardCategories[index].name
           }
       }

}
