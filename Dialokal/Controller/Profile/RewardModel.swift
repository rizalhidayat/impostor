//
//  RewardModel.swift
//  Locla
//
//  Created by Edward da Costa on 11/08/20.
//  Copyright © 2020 Apple Developer Academy. All rights reserved.
//

import Foundation

public struct Reward {
    var name : String
    var lokasi : String
    var imageRewardName : String
    var descReward : String
    var sourceIMG : String
}

