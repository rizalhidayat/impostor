//
//  RewardCollectionViewCell.swift
//  Locla
//
//  Created by Rizal Hidayat on 15/08/20.
//  Copyright © 2020 Apple Developer Academy. All rights reserved.
//

import UIKit

class RewardCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var rewardIV: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        layer.borderWidth = 1.0
        layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    }
}
