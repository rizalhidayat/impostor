//
//  ChallengeObrolanVC.swift
//  Locla
//
//  Created by Bagus setiawan on 13/08/20.
//  Copyright © 2020 Apple Developer Academy. All rights reserved.
//

import UIKit
import AVFoundation

class ChallengeObrolanVC: UIViewController, AVAudioPlayerDelegate {
    
    @IBOutlet weak var messageTableView: UITableView!
    @IBOutlet weak var recapTableView: UITableView!
    
    @IBOutlet weak var profilIV: UIImageView!
    @IBOutlet weak var nameProfilLbl: UILabel!
    @IBOutlet weak var identityProfilLbl: UILabel!
    
    @IBOutlet weak var viewInputUser: UIView!
    @IBOutlet weak var failedNotification1Lbl: UILabel!
    @IBOutlet weak var failedNotification2Lbl: UILabel!
    @IBOutlet weak var selfChatLbl: UILabel!
    
    @IBOutlet weak var submitBtn: UIButton!
    @IBOutlet weak var answerABtn: UIButton!
    @IBOutlet weak var answerBBtn: UIButton!
    @IBOutlet weak var answerCBtn: UIButton!
    @IBOutlet weak var answerDBtn: UIButton!
    @IBOutlet weak var closeButton: UIButton!
    
    @IBOutlet weak var resultButton: UIButton!
    @IBOutlet weak var blurView: UIView!
    @IBOutlet weak var recapView: UIView!
    @IBOutlet weak var recapButton: UIButton!
    
    var subtopicID : Int?
    var chatChallenges : [ChatChallenge] = []
    var add : [ChatChallenge] = []
    var recapChat : [ChatChallenge] = []
    
    var colors : [UIColor] = [#colorLiteral(red: 0.968627451, green: 0.7843137255, blue: 0.262745098, alpha: 1),#colorLiteral(red: 0, green: 0.768627451, blue: 0.7254901961, alpha: 1),#colorLiteral(red: 0.6941176471, green: 0, blue: 0, alpha: 1),#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1),#colorLiteral(red: 0.6823529412, green: 0.6823529412, blue: 0.6941176471, alpha: 1),#colorLiteral(red: 0.4901960784, green: 0.1411764706, blue: 0.2274509804, alpha: 1)]
    var totalQuestion : Int = 0
    var currentQuestion : Int = 1
    var selfChatText = ""
    var tempSelfChatText = ""
    var choiceA = ""
    var choiceB = ""
    var choiceC = ""
    var choiceD = ""
    var answerFill1 = ""
    var answerFill2 = ""
    var userChoice = ""
    var userChoice1 = "#"
    var userChoice2 = "#"
    var indexBlank1 = 0
    var indexBlank2 = 0
    var answer1 = 0
    var answer2 = 0
    var userAnswer = ""
    var userAnswer1 = ""
    var userAnswer2 = ""
    var index = 0
    var starGained = 0
    var clickedWrong = false
    var firstTry = 0
    var counterWrong = 0
    
    //var delayDuration = 0.0
    var chatSoundName : String = ""
    var player : AVAudioPlayer?
    
    var recapStatus = false
    
    //    var currentTableAnimation: TableAnimation = .fadeIn(duration: 0.85, delay: 0.03)
    //    var animationDuration: TimeInterval = 0.85
    //    var delay: TimeInterval = 2.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        fetchChatCallenge()
        setRowHeight()
        setupHeadView()
        messageTableView.dataSource = self
        messageTableView.delegate = self
        recapTableView.dataSource = self
        recapTableView.delegate = self
        setupTableView()
        configureButton()
        configureFailedNotif()
        configureKeterangan()
        configureAnswer()
        configureInputUser()
        configureLabel()
        checkIndexBlank()
        tempSelfChatText = selfChatText
        messageTableView.isHidden = false
        recapTableView.isHidden = true
        add.append(chatChallenges[0])
    }
    
    func setRowHeight(){
        self.messageTableView.rowHeight = UITableView.automaticDimension
        self.messageTableView.estimatedRowHeight = 120.0
        self.recapTableView.rowHeight = UITableView.automaticDimension
        self.recapTableView.estimatedRowHeight = 120.0
    }
    
    func setupHeadView(){
        self.profilIV.image = UIImage(named: "People") //ubah nanti
        self.profilIV.layer.cornerRadius = 27.0
        self.profilIV.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.profilIV.layer.borderWidth = 2.0
        self.nameProfilLbl.text = chatChallenges[0].opponentName
        self.identityProfilLbl.text = chatChallenges[0].opponentStatus
    }
    
    func fetchChatCallenge() {
        chatChallenges = DataLoader.getChatChallenges(subtopicID: subtopicID ?? 2)
        self.totalQuestion = chatChallenges.count
        self.selfChatText = self.chatChallenges[currentQuestion-1].selfChatText ?? ""
        self.answerFill1 = self.chatChallenges[currentQuestion-1].fill1 ?? ""
        self.answerFill2 = self.chatChallenges[currentQuestion-1].fill2 ?? ""
        self.tempSelfChatText = self.selfChatText
        self.choiceA = self.chatChallenges[currentQuestion-1].optionA ?? ""
        self.choiceB = self.chatChallenges[currentQuestion-1].optionB ?? ""
        self.choiceC = self.chatChallenges[currentQuestion-1].optionC ?? ""
        self.choiceD = self.chatChallenges[currentQuestion-1].optionD ?? ""
    }
    
    func getTotalStar()->Int{
        var total = 0
        if self.clickedWrong == false {
            self.starGained += 3
            self.firstTry += 1
            total = 3
        }else
        if self.clickedWrong == true  && self.counterWrong >= 2{
            self.starGained += 1
            total = 1
        }else
        if self.clickedWrong == true  && self.counterWrong == 1{
            self.starGained += 2
            total = 2
        }
        return total
    }
    
    func setupTableView()
    {
        rightMessageRegister()
        leftMessageRegister()
    }
    
    func rightMessageRegister() {
        let nib = UINib(nibName: RightMessage.cellID, bundle: Bundle.main)
        self.messageTableView.register(nib, forCellReuseIdentifier: RightMessage.cellID)
        self.recapTableView.register(nib, forCellReuseIdentifier: RightMessage.cellID)
    }
    
    func leftMessageRegister() {
        let nib = UINib(nibName: LeftMessage.cellID, bundle: Bundle.main)
        self.messageTableView.register(nib, forCellReuseIdentifier: LeftMessage.cellID)
        self.recapTableView.register(nib, forCellReuseIdentifier: LeftMessage.cellID)
    }
    
    
    func configureFailedNotif(){
        self.failedNotification1Lbl.isHidden = true
        self.failedNotification2Lbl.isHidden = true
    }
    
    func configureButton(){
        self.answerABtn.titleLabel?.adjustsFontSizeToFitWidth = true
        self.answerBBtn.titleLabel?.adjustsFontSizeToFitWidth = true
        self.answerCBtn.titleLabel?.adjustsFontSizeToFitWidth = true
        self.answerDBtn.titleLabel?.adjustsFontSizeToFitWidth = true
        
        
        self.submitBtn.layer.cornerRadius  = 20.0
        self.submitBtn.layer.borderWidth = 1.0
        self.submitBtn.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        
        self.answerABtn.layer.cornerRadius = 17.5
        self.answerABtn.layer.borderWidth = 1.0
        self.answerABtn.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        
        self.answerBBtn.layer.cornerRadius = 17.5
        self.answerBBtn.layer.borderWidth = 1.0
        self.answerBBtn.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        
        self.answerCBtn.layer.cornerRadius = 17.5
        self.answerCBtn.layer.borderWidth = 1.0
        self.answerCBtn.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        
        self.answerDBtn.layer.cornerRadius = 17.5
        self.answerDBtn.layer.borderWidth = 1.0
        self.answerDBtn.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        
        self.closeButton.layer.cornerRadius = 15.0
        self.closeButton.layer.borderWidth = 1.0
        self.closeButton.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    }
    
    func configureKeterangan(){
        self.failedNotification1Lbl.isHidden = true
        self.failedNotification2Lbl.isHidden = true
        self.counterWrong = 0
    }
    
    func configureLabel(){
        selfChatLbl.text = selfChatText.replacingOccurrences(of: "#", with: "____")
    }
    
    func configureAnswer(){
        self.answerABtn.setTitle(self.choiceA, for: .normal)
        self.answerBBtn.setTitle(self.choiceB, for: .normal)
        self.answerCBtn.setTitle(self.choiceC, for: .normal)
        self.answerDBtn.setTitle(self.choiceD, for: .normal)
        
    }
    
    func configureInputUser(){
        self.viewInputUser.layer.cornerRadius = 21.0
        self.viewInputUser.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.viewInputUser.layer.borderWidth = 2.0
        
        self.resultButton.layer.cornerRadius = 26.5
        self.resultButton.layer.borderWidth = 2.0
        self.resultButton.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        
        self.recapView.layer.cornerRadius = 26.5
        self.recapView.layer.borderWidth = 2.0
        self.recapView.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        
        self.recapButton.layer.cornerRadius = 26.5
        self.recapButton.layer.borderWidth = 2.0
        self.recapButton.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        
        self.blurView.isHidden = true
    }
    
    func checkTotalBlank () -> Int {
        var totalBlank = 0
        let array = selfChatText.components(separatedBy: " ")
        for index in 1...array.count {
            if(array[index-1] == "#"){
                totalBlank += 1
            }
        }
        return totalBlank
    }
    
    
    func activeFilledBlank () -> Int {
        var activeButton = 0
        if answerABtn.backgroundColor == colors[4] {
            activeButton += 1
        }
        if answerBBtn.backgroundColor == colors[4] {
            activeButton += 1
        }
        if answerCBtn.backgroundColor == colors[4] {
            activeButton += 1
        }
        if answerDBtn.backgroundColor == colors[4] {
            activeButton += 1
        }
        return activeButton
    }
    
    func checkIndexBlank(){
        let arrays = selfChatText.components(separatedBy: " ")
        var totalNotFilled = 0
        
        for index in 1...arrays.count {
            if totalNotFilled == 0 && arrays[index-1] == "#"{
                self.indexBlank1 = index-1
                totalNotFilled += 1
            }else
            if totalNotFilled == 1 && arrays[index-1] == "#"{
                self.indexBlank2 = index-1
                totalNotFilled += 1
            }
        }
    }
    
    func clickedButton(){
        var tempSelfChatTextArray = tempSelfChatText.components(separatedBy: " ")
        tempSelfChatTextArray[indexBlank1] = userChoice1
        tempSelfChatTextArray[indexBlank2] = userChoice2
        tempSelfChatText = tempSelfChatTextArray.joined(separator: " ")
        selfChatLbl.text = tempSelfChatText.replacingOccurrences(of: "#", with: "_")
    }
    
    
    @IBAction func didTapOptionAButton(_ sender: Any) {
        
        if checkTotalBlank() == 1{
            if activeFilledBlank() == 0{
                self.answerABtn.backgroundColor = colors[4]
                selfChatLbl.text = selfChatText.replacingOccurrences(of: "#", with: choiceA)
                self.userChoice = choiceA
                self.userAnswer = "A"
            }
            else if (activeFilledBlank() == 1) && (self.answerABtn.backgroundColor == colors[4]){
                self.answerABtn.backgroundColor = colors[0]
                selfChatLbl.text = selfChatText.replacingOccurrences(of: "#", with: "_")
                self.userChoice = ""
                self.userAnswer = ""
            }
        }
        
        if checkTotalBlank() == 2{
            if userChoice1 == "#" && userChoice2 == "#" {
                self.userChoice1 = choiceA
                self.userAnswer1 = "A"
                self.answerABtn.backgroundColor = colors[4]
                clickedButton()
            }
            else
            if userChoice1 != "#" && userChoice2 == "#" {
                if userChoice1 == choiceA{
                    self.userChoice1 = "#"
                    self.userAnswer1 = ""
                    self.answerABtn.backgroundColor = colors[0]
                    clickedButton()
                }else
                if userChoice1 != choiceA{
                    self.userChoice2 = choiceA
                    self.userAnswer2 = "A"
                    self.answerABtn.backgroundColor = colors[4]
                    clickedButton()
                }
            }
            else
            if userChoice1 == "#" && userChoice2 != "#" {
                if userChoice2 == choiceA{
                    self.userChoice2 = "#"
                    self.userAnswer2 = ""
                    self.answerABtn.backgroundColor = colors[0]
                    clickedButton()
                }else
                if userChoice2 != choiceA{
                    self.userChoice1 = choiceA
                    self.userAnswer1 = "A"
                    self.answerABtn.backgroundColor = colors[4]
                    clickedButton()
                }
            }
            else
            if userChoice1 != "#" && userChoice2 != "#" {
                if userChoice1 == choiceA{
                    self.userChoice1 = "#"
                    self.userAnswer1 = ""
                    self.answerABtn.backgroundColor = colors[0]
                    clickedButton()
                }else
                if userChoice2 == choiceA{
                    self.userChoice2 = "#"
                    self.userAnswer2 = ""
                    self.answerABtn.backgroundColor = colors[0]
                    clickedButton()
                }
            }
        }
        
        
    }
    
    @IBAction func didTapOptionBButton(_ sender: Any) {
        
        if checkTotalBlank() == 1{
            if activeFilledBlank() == 0{
                self.answerBBtn.backgroundColor = colors[4]
                selfChatLbl.text = selfChatText.replacingOccurrences(of: "#", with: choiceB)
                self.userChoice = choiceB
                self.userAnswer = "B"
            }else
            if (activeFilledBlank() == 1) && (self.answerBBtn.backgroundColor == colors[4]){
                self.answerBBtn.backgroundColor = colors[0]
                selfChatLbl.text = selfChatText.replacingOccurrences(of: "#", with: "_")
                self.userChoice = ""
                self.userAnswer = ""
            }
        }
        
        if checkTotalBlank() == 2{
            if userChoice1 == "#" && userChoice2 == "#" {
                self.userChoice1 = choiceB
                self.userAnswer1 = "B"
                self.answerBBtn.backgroundColor = colors[4]
                clickedButton()
            }
            else
            if userChoice1 != "#" && userChoice2 == "#" {
                if userChoice1 == choiceB{
                    self.userChoice1 = "#"
                    self.userAnswer1 = ""
                    self.answerBBtn.backgroundColor = colors[0]
                    clickedButton()
                }else
                if userChoice1 != choiceB{
                    self.userChoice2 = choiceB
                    self.userAnswer2 = "B"
                    self.answerBBtn.backgroundColor = colors[4]
                    clickedButton()
                }
            }
            else
            if userChoice1 == "#" && userChoice2 != "#" {
                if userChoice2 == choiceB{
                    self.userChoice2 = "#"
                    self.userAnswer2 = ""
                    self.answerBBtn.backgroundColor = colors[0]
                    clickedButton()
                }else if userChoice2 != choiceB{
                    self.userChoice1 = choiceB
                    self.userAnswer1 = "B"
                    self.answerBBtn.backgroundColor = colors[4]
                    clickedButton()
                }
            }
            else
            if userChoice1 != "#" && userChoice2 != "#" {
                if userChoice1 == choiceB{
                    self.userChoice1 = "#"
                    self.userAnswer1 = ""
                    self.answerBBtn.backgroundColor = colors[0]
                    clickedButton()
                }else
                
                if userChoice2 == choiceB{
                    self.userChoice2 = "#"
                    self.userAnswer2 = ""
                    self.answerBBtn.backgroundColor = colors[0]
                    clickedButton()
                }
            }
            
        }
        
    }
    
    @IBAction func didTapOptionCButton(_ sender: Any) {
        
        if checkTotalBlank() == 1 {
            if activeFilledBlank() == 0{
                self.answerCBtn.backgroundColor = colors[4]
                selfChatLbl.text = selfChatText.replacingOccurrences(of: "#", with: choiceC)
                self.userChoice = choiceC
                self.userAnswer = "C"
            }else
            if (activeFilledBlank() == 1) && (self.answerCBtn.backgroundColor == colors[4]){
                self.answerCBtn.backgroundColor = colors[0]
                selfChatLbl.text = selfChatText.replacingOccurrences(of: "#", with: "_")
                self.userChoice = ""
                self.userAnswer = ""
            }
        }
        
        if checkTotalBlank() == 2{
            if userChoice1 == "#" && userChoice2 == "#" {
                self.userChoice1 = choiceC
                self.userAnswer1 = "C"
                self.answerCBtn.backgroundColor = colors[4]
                clickedButton()
            }
            else
            if userChoice1 != "#" && userChoice2 == "#" {
                if userChoice1 == choiceC{
                    self.userChoice1 = "#"
                    self.userAnswer1 = ""
                    self.answerCBtn.backgroundColor = colors[0]
                    clickedButton()
                }else
                if userChoice1 != choiceC{
                    self.userChoice2 = choiceC
                    self.userAnswer2 = "C"
                    self.answerCBtn.backgroundColor = colors[4]
                    clickedButton()
                }
            }else
            
            if userChoice1 == "#" && userChoice2 != "#" {
                if userChoice2 == choiceC{
                    self.userChoice2 = "#"
                    self.userAnswer2 = ""
                    self.answerCBtn.backgroundColor = colors[0]
                    clickedButton()
                }else
                
                if userChoice2 != choiceC{
                    self.userChoice1 = choiceC
                    self.userAnswer1 = "C"
                    self.answerCBtn.backgroundColor = colors[4]
                    clickedButton()
                }
            }
            else
            if userChoice1 != "#" && userChoice2 != "#" {
                if userChoice1 == choiceC{
                    self.userChoice1 = "#"
                    self.userAnswer1 = ""
                    self.answerCBtn.backgroundColor = colors[0]
                    clickedButton()
                }else
                
                if userChoice2 == choiceC{
                    self.userChoice2 = "#"
                    self.userAnswer2 = ""
                    self.answerCBtn.backgroundColor = colors[0]
                    clickedButton()
                }
            }
        }
        
    }
    
    @IBAction func didTapOptionDButton(_ sender: Any) {
        
        if checkTotalBlank() == 1{
            if activeFilledBlank() == 0{
                self.answerDBtn.backgroundColor = colors[4]
                selfChatLbl.text = selfChatText.replacingOccurrences(of: "#", with: choiceD)
                self.userChoice = choiceD
                self.userAnswer = "D"
            }else
            if (activeFilledBlank() == 1) && (self.answerDBtn.backgroundColor == colors[4]){
                self.answerDBtn.backgroundColor = colors[0]
                selfChatLbl.text = selfChatText.replacingOccurrences(of: "#", with: "_")
                self.userChoice = ""
                self.userAnswer = ""
            }
        }
        
        
        if checkTotalBlank() == 2{
            if userChoice1 == "#" && userChoice2 == "#" {
                self.userChoice1 = choiceD
                self.userAnswer1 = "D"
                self.answerDBtn.backgroundColor = colors[4]
                clickedButton()
            }else
            
            if userChoice1 != "#" && userChoice2 == "#" {
                if userChoice1 == choiceD{
                    self.userChoice1 = "#"
                    self.userAnswer1 = ""
                    self.answerDBtn.backgroundColor = colors[0]
                    clickedButton()
                }else
                if userChoice1 != choiceD{
                    self.userChoice2 = choiceD
                    self.userAnswer2 = "D"
                    self.answerDBtn.backgroundColor = colors[4]
                    clickedButton()
                }
            }else
            
            if userChoice1 == "#" && userChoice2 != "#" {
                if userChoice2 == choiceD{
                    self.userChoice2 = "#"
                    self.userAnswer2 = ""
                    self.answerDBtn.backgroundColor = colors[0]
                    clickedButton()
                }else
                
                if userChoice2 != choiceD{
                    self.userChoice1 = choiceD
                    self.userAnswer1 = "D"
                    self.answerDBtn.backgroundColor = colors[4]
                    clickedButton()
                }
            }
            else
            if userChoice1 != "#" && userChoice2 != "#" {
                if userChoice1 == choiceD{
                    self.userChoice1 = "#"
                    self.userAnswer1 = ""
                    self.answerDBtn.backgroundColor = colors[0]
                    clickedButton()
                }else
                
                if userChoice2 == choiceD{
                    self.userChoice2 = "#"
                    self.userAnswer2 = ""
                    self.answerDBtn.backgroundColor = colors[0]
                    clickedButton()
                }
            }
        }
        
    }
    @IBAction func didTapRecapButton(_ sender: Any) {
        enableButtonDuringRecap(status: false)
        self.messageTableView.isHidden = true
        self.recapTableView.isHidden = false
        self.recapChat.removeAll()
        self.recapTableView.reloadData()
        scrollToTop()
        recapAction()
        
    }
    
    func enableButtonDuringRecap(status : Bool){
        //self.recapButton.backgroundColor = .secondarySystemBackground
        self.recapButton.isEnabled = status
        
        //self.closeButton.backgroundColor = .secondarySystemBackground
        self.closeButton.isEnabled = status
        
        //self.resultButton.backgroundColor = .secondarySystemBackground
        self.resultButton.isEnabled = status
        if status {
            self.resultButton.backgroundColor = UIColor(named: "PrimaryYellow")
            self.recapButton.backgroundColor = UIColor(named: "PrimaryYellow")
        } else {
            self.resultButton.backgroundColor = UIColor(named: "InactiveYellow")
            self.recapButton.backgroundColor = UIColor(named: "InactiveYellow")
        }
    }
    
    func scrollToTop() {
        let topRow = IndexPath(row: 0, section: 0)
        self.messageTableView.scrollToRow(at: topRow, at: .top, animated: true)
    }
    
    func recapAction(){
        var delayDuration : [Double] = []
        var counter = 0.0
        for i in 1...add.count {
            delayDuration.append(getDuration(index: i))
        }
        
        for index in 1...add.count {
            DispatchQueue.main.asyncAfter(deadline: .now() + counter ) {
                print("counter -> \(counter)")
                if index % 2 == 0 {
                    self.chatSoundName = self.add[index-1].selfSoundFilename ?? "chat_1_2_1"
                }else{
                    self.chatSoundName = self.add[index-1].opponentSoundFilename ?? "chat_1_2_1"
                    //self.chatSoundName = "chat_1_2_1"
                }
                
                let chatSoundNameURL = Bundle.main.url(forResource: self.chatSoundName, withExtension: "mp3")!
                
                do {
                    try self.player = AVAudioPlayer(contentsOf: chatSoundNameURL)
                } catch {
                    print(error)
                }
                self.recapChat.append(self.add[index-1])
                self.recapTableView.reloadData()
                self.recapTableView.scrollToBottomRow()
                self.player?.play()
                print("Durasi -> \(self.player?.duration ?? 1)")
                print("row \(index-1)")
                print("play sound")
                
                if index == self.add.count {
                    self.enableButtonDuringRecap(status: true)
                }
            }
            counter = counter + delayDuration[index-1]
            //            perform(#selector(delayedFunction(index:)), with: nil, afterDelay: counter)
            //counter += 4.0
            //            print("increment delay")
        }
    }
    
    func getDuration(index : Int) -> Double{
        if index % 2 == 0 {
            self.chatSoundName = self.add[index-1].selfSoundFilename ?? "chat_1_2_1"
            //self.chatSoundName = "chat_1_2_1"
        }else{
            self.chatSoundName = self.add[index-1].opponentSoundFilename ?? "chat_1_2_1"
            //self.chatSoundName = "chat_1_2_1"
        }
        //guard let chatSoundNameURL = Bundle.main.url(forResource: self.chatSoundName, withExtension: "mp3") else { return }
        let chatSoundNameURL = Bundle.main.url(forResource: self.chatSoundName, withExtension: "mp3")!
        
        do {
            try self.player = AVAudioPlayer(contentsOf: chatSoundNameURL)
        } catch {
            print(error)
        }
        return self.player?.duration ?? 1
    }
    
    @IBAction func didTapResultButton(_ sender: Any) {
        self.player?.stop()
        resultPage()
    }
    
    func checkAnswer()->Bool{
        var answer = false
        if checkTotalBlank() == 1{
            if self.userAnswer == answerFill1{
                answer = true
            }
        }else
        if checkTotalBlank() == 2 {
            if ((self.userAnswer1 == answerFill1) && (self.userAnswer2 == answerFill2)){
                answer = true
            }
        }
        return answer
    }
    
    func rightAnswer(){
        let alertVC = UIStoryboard(name: "CustomAlert", bundle: nil).instantiateViewController(withIdentifier: "AlertStarVC") as! AlertStarVC
        alertVC.starCount = getTotalStar()
        alertVC.tip = self.chatChallenges[currentQuestion-1].tips ?? "Error"
        alertVC.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        
        alertVC.parentID = 2
        alertVC.delegateFromChat = self
        self.addChild(alertVC)
        self.view.addSubview(alertVC.view)
    }
    
    func wrongAnswer(){
        self.failedNotification1Lbl.isHidden = false
        self.failedNotification2Lbl.isHidden = false
        if answerABtn.backgroundColor == colors[4] {
            answerABtn.backgroundColor = colors[2]
        }
        if answerBBtn.backgroundColor == colors[4] {
            answerBBtn.backgroundColor = colors[2]
        }
        if answerCBtn.backgroundColor == colors[4] {
            answerCBtn.backgroundColor = colors[2]
        }
        if answerDBtn.backgroundColor == colors[4] {
            answerDBtn.backgroundColor = colors[2]
        }
        self.clickedWrong = true
        self.counterWrong += 1
    }
    
    func configureChallengeState(){
        self.clickedWrong = false
    }
    
    func insertAnswerToChat() {
        
        add.append(chatChallenges[index])
        
        if chatChallenges.indices.contains(index + 1) {
            add.append(chatChallenges[index + 1])
        }
        
        index += 1
        
        messageTableView.reloadData()
        messageTableView.scrollToBottomRow()
    }
    
    @IBAction func didTapSubmitButton(_ sender: Any) {
        if checkAnswer() == true{
            rightAnswer()
            self.currentQuestion += 1
            insertAnswerToChat()
            
        }else if checkAnswer() == false{
            wrongAnswer()
        }
    }
    
    
    @IBAction func didTapCloseBtn(_ sender: Any) {
        self.player?.stop()
        let alertVC = UIStoryboard(name: "CustomAlert", bundle: nil).instantiateViewController(withIdentifier: "AlertExitVC") as! AlertExitVC
        
        alertVC.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        self.addChild(alertVC)
        self.view.addSubview(alertVC.view)
        
    }
    
    func recapPage(){
        self.viewInputUser.isHidden = true
        let alertVC = UIStoryboard(name: "CustomAlert", bundle: nil).instantiateViewController(withIdentifier: "AlertRecapVC") as! AlertRecapVC
        
        alertVC.view.backgroundColor = UIColor.black.withAlphaComponent(0.0)
        alertVC.delegate = self
        self.addChild(alertVC)
        self.view.addSubview(alertVC.view)
    }
    
    func resultPage(){
        let storyboard = UIStoryboard(name: "Result", bundle: nil)
        let destination = storyboard.instantiateViewController(identifier: "ResultVC") as! ResultVC
        destination.modalPresentationStyle = .fullScreen
        destination.modalTransitionStyle = .crossDissolve
        destination.subtopik = DataLoader.getSubtopic(subtopicID: self.subtopicID ?? 2)
        destination.starGained = self.starGained
        destination.firstTry = self.firstTry
        destination.totalQuestion = self.totalQuestion
        self.present(destination, animated: true, completion: nil)
    }
}

extension ChallengeObrolanVC: UITableViewDataSource, UITableViewDelegate{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == messageTableView {
            return add.count
        }
        if tableView == recapTableView {
            return recapChat.count
        }
        return add.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if (indexPath.row % 2 == 0){
            let cell2 = tableView.dequeueReusableCell(withIdentifier: LeftMessage.cellID) as! LeftMessage
            if tableView == messageTableView {
                let message = add[indexPath.row]
                cell2.messageLeftLabel.text = message.opponentChatTextLocal
                cell2.messageLeftSecondaryLabel.text = message.opponentChatTextIndo
                return cell2
            }
            if tableView == recapTableView {
                let message = recapChat[indexPath.row]
                cell2.messageLeftLabel.text = message.opponentChatTextLocal
                cell2.messageLeftSecondaryLabel.text = message.opponentChatTextIndo
                return cell2
            }
            return cell2
        } else {
            let cell1 = tableView.dequeueReusableCell(withIdentifier: RightMessage.cellID) as! RightMessage
            if tableView == messageTableView {
                let message = add[indexPath.row - 1]
                cell1.messageRightLabel.text = message.selfChatTextLocal
                cell1.messageRightSecondaryLabrl.text = message.selfChatTextIndo
                return cell1
            }
            if tableView == recapTableView {
                let message = recapChat[indexPath.row - 1]
                cell1.messageRightLabel.text = message.selfChatTextLocal
                cell1.messageRightSecondaryLabrl.text = message.selfChatTextIndo
                return cell1
            }
            return cell1
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

extension ChallengeObrolanVC: AlertStarVCDelegate{
    func reload() {
        
        if currentQuestion <= totalQuestion{
            resetChallengeAttribute()
            fetchChatCallenge()
            self.failedNotification1Lbl.isHidden = true
            self.failedNotification2Lbl.isHidden = true
            if self.answerABtn.backgroundColor != colors[0] {
                answerABtn.backgroundColor = colors[0]
            }
            if answerBBtn.backgroundColor != colors[0] {
                answerBBtn.backgroundColor = colors[0]
            }
            if answerCBtn.backgroundColor != colors[0] {
                answerCBtn.backgroundColor = colors[0]
            }
            if answerDBtn.backgroundColor != colors[0] {
                answerDBtn.backgroundColor = colors[0]
            }
            configureChallengeState()
            configureButton()
            configureFailedNotif()
            configureKeterangan()
            configureAnswer()
            configureInputUser()
            configureLabel()
            checkIndexBlank()
            
            if chatChallenges[index].selfChatText == "" {
                self.viewInputUser.isHidden = true
                self.blurView.isHidden = false
            }
            
        }
        
        if currentQuestion > totalQuestion{
            self.viewInputUser.isHidden = true
            self.blurView.isHidden = false
        }
    }
    
    func resetChallengeAttribute(){
        self.selfChatText = ""
        self.tempSelfChatText = ""
        self.choiceA = ""
        self.choiceB = ""
        self.choiceC = ""
        self.choiceD = ""
        self.answerFill1 = ""
        self.answerFill2 = ""
        self.userChoice = ""
        self.userChoice1 = "#"
        self.userChoice2 = "#"
        self.indexBlank1 = 0
        self.indexBlank2 = 0
        self.answer1 = 0
        self.answer2 = 0
        self.userAnswer = ""
        self.userAnswer1 = ""
        self.userAnswer2 = ""
    }
}

extension ChallengeObrolanVC: AlertRecapVCDelegate{
    func clickedRecapButton() {
        
    }
    
    func clickedResultButton() {
        resultPage()
    }
    
}
