//
//  recapObrolanVC.swift
//  Locla
//
//  Created by Bagus setiawan on 18/08/20.
//  Copyright © 2020 Apple Developer Academy. All rights reserved.
//

import UIKit

class RecapObrolanVC: UIViewController {
    //add comment
    @IBOutlet weak var profilImageView: UIImageView!
    @IBOutlet weak var nameProfilLabel: UILabel!
    @IBOutlet weak var statusNameProfilLabel: UILabel!
    
    @IBOutlet weak var chatTableView: UITableView!
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var lanjutkanButton: UIButton!
    
    var subtopicID : Int?
    var listChat : [ChatChallenge] = []
    var addCell : [ChatChallenge] = []
    
    var colors : [UIColor] = [#colorLiteral(red: 0.9764705882, green: 0.6156862745, blue: 0.01176470588, alpha: 1),#colorLiteral(red: 0.3098039216, green: 0.4196078431, blue: 0.862745098, alpha: 1),#colorLiteral(red: 0.5215686275, green: 0.368627451, blue: 0.8470588235, alpha: 1),#colorLiteral(red: 0.9803921569, green: 0.3921568627, blue: 0, alpha: 1),#colorLiteral(red: 0.5568627451, green: 0.7490196078, blue: 0.4039215686, alpha: 1),#colorLiteral(red: 0.4901960784, green: 0.1411764706, blue: 0.2274509804, alpha: 1)]
    var totalQuestion : Int = 0
    var currentQuestion : Int = 1
    var selfChatText = ""
    var tempSelfChatText = ""
    var index = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        fetchChatCallenge()
        setupTableView()
        setupButton()
        chatTableView.delegate = self
        chatTableView.dataSource = self
        addCell.append(listChat[0])
        // Do any additional setup after loading the view.
    }
    
    func fetchChatCallenge() {
        listChat = DataLoader.getChatChallenges(subtopicID: subtopicID ?? 2)
        self.totalQuestion = listChat.count
        self.selfChatText = self.listChat[currentQuestion-1].selfChatText ?? ""
        self.tempSelfChatText = self.selfChatText
        
    }
    
    func setupButton(){
        playButton.backgroundColor = colors[0]
        playButton.layer.cornerRadius = 10
        
        
        lanjutkanButton.backgroundColor = .clear
        lanjutkanButton.layer.cornerRadius = 10
        lanjutkanButton.layer.borderWidth = 2
        lanjutkanButton.layer.borderColor = #colorLiteral(red: 1, green: 0.5720084906, blue: 0, alpha: 1)
    }
    
    func setupTableView()
    {
        rightMessageRegister()
        leftMessageRegister()
    }
    
    func rightMessageRegister() {
        let nib = UINib(nibName: RightMessage.cellID, bundle: Bundle.main)
        self.chatTableView.register(nib, forCellReuseIdentifier: RightMessage.cellID)
    }
    
    func leftMessageRegister() {
        let nib = UINib(nibName: LeftMessage.cellID, bundle: Bundle.main)
        self.chatTableView.register(nib, forCellReuseIdentifier: LeftMessage.cellID)
    }
    
    func insertAnswerToChat() {
        
        addCell.append(listChat[index])
        if listChat.indices.contains(index + 1) {
            addCell.append(listChat[index + 1])
        }
        index += 1
        
        chatTableView.reloadData()
        
        let max = listChat.count
        
        if index == max {
            let storyboard = UIStoryboard(name: "Obrolan", bundle: nil)
            let destination = storyboard.instantiateViewController(identifier: "ObrolanResultVC") as! ObrolanResultVC
            destination.modalPresentationStyle = .fullScreen
            destination.modalTransitionStyle = .crossDissolve
            self.present(destination, animated: true, completion: nil)
        }
        
    }
    
    @IBAction func playChat(_ sender: Any) {
        insertAnswerToChat()
        
        
    }
    
    @IBAction func diRdTapCloseBtn(_ sender: Any) {
        
        
        let alertVC = UIStoryboard(name: "CustomAlert", bundle: nil).instantiateViewController(withIdentifier: "AlertExitVC") as! AlertExitVC
        
        alertVC.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        self.addChild(alertVC)
        self.view.addSubview(alertVC.view)
        
    }
    
    @IBAction func lanjutkanButton(_ sender: Any) {
        lanjutkanButton.backgroundColor = #colorLiteral(red: 1, green: 0.5720084906, blue: 0, alpha: 1)
        
        let storyboard = UIStoryboard(name: "Obrolan", bundle: nil)
        let destination = storyboard.instantiateViewController(identifier: "ObrolanResultVC") as! ObrolanResultVC
        destination.modalPresentationStyle = .fullScreen
        destination.modalTransitionStyle = .crossDissolve
        self.present(destination, animated: true, completion: nil)
    }
}

extension RecapObrolanVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return addCell.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (indexPath.row % 2 == 0){
            let message = addCell[indexPath.row]
            let cell2 = tableView.dequeueReusableCell(withIdentifier: LeftMessage.cellID) as! LeftMessage
            cell2.messageLeftLabel.text = message.opponentChatTextLocal
            cell2.messageLeftSecondaryLabel.text = message.opponentChatTextIndo
            
            return cell2

        } else {
            let message = addCell[indexPath.row - 1]
            let cell1 = tableView.dequeueReusableCell(withIdentifier: RightMessage.cellID) as! RightMessage
            cell1.messageRightLabel.text = message.selfChatTextIndo
            cell1.messageRightSecondaryLabrl.text = message.selfChatTextIndo
            return cell1
        }
    }
    
    
}
