//
//  ObrolanResultVC.swift
//  Locla
//
//  Created by Bagus setiawan on 18/08/20.
//  Copyright © 2020 Apple Developer Academy. All rights reserved.
//

import UIKit

class ObrolanResultVC: UIViewController {
    
    @IBOutlet weak var subtopicImageView: UIImageView!
    @IBOutlet weak var resultIndoLabel: UILabel!
    @IBOutlet weak var resultLocalLabel: UILabel!
    @IBOutlet weak var star1ImageView: UIImageView!
    @IBOutlet weak var star2ImageView: UIImageView!
    @IBOutlet weak var star3ImgeView: UIImageView!
    @IBOutlet weak var jempolImageView: UIImageView!
    @IBOutlet weak var percobaanPertamaLabel: UILabel!
    @IBOutlet weak var randomTipLabel: UILabel!
    @IBOutlet weak var seleseiButton: UIButton!
    @IBOutlet weak var viewRandomTip: UIView!
    @IBOutlet weak var totalBintangImageview: UIImageView!
    @IBOutlet weak var totalBintangLabel: UILabel!
    
    @IBOutlet weak var collectionView : UICollectionView!
    
    var subtopicID : Int?
    var chatChallenges : [ChatChallenge] = []
    var subtopik : Subtopic!
       var vocabularies : [Vocabulary] = []
       var starGained = 0
       var firstTry = 0
       var totalQuestion = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        DataLoader.updateSubtopic(subtopicId: subtopik.id, star: self.starGained, firstTry: self.firstTry)
        configureImage()
        configureLabel()
        configureButton()
        // Do any additional setup after loading the view.
    }
    
    func fetchChatCallenge() {
        chatChallenges = DataLoader.getChatChallenges(subtopicID: subtopicID ?? 2)
    }
    
    func configureButton(){
        self.seleseiButton.layer.cornerRadius = 10.0
    }
    
    func configureLabel(){
        self.resultIndoLabel.text = "Kamu yang terbaik!!"
        self.resultLocalLabel.text = "Kon sangar"
        self.viewRandomTip.layer.cornerRadius = 10.0
        self.percobaanPertamaLabel.text = "\(self.firstTry)/\(self.totalQuestion)"
        self.totalBintangLabel.text = "\(self.starGained)/\(self.totalQuestion*3)"
        let locationID : Int = DataLoader.getCurrentLocation()
        self.randomTipLabel.text = DataLoader.getRandomTip(locationID: locationID)
        self.resultLocalLabel.isHidden = true
    }
    
    func configureImage(){
        self.subtopicImageView.image = UIImage(named: "People1")
        self.star1ImageView.image = UIImage(named: "star")
        self.star2ImageView.image = UIImage(named: "star")
        self.star3ImgeView.image = UIImage(named: "star")
        self.jempolImageView.image = UIImage(named: "Image-1")
    }
    
    
    
    @IBAction func didTapSeleseiButton(_ sender: Any) {
       // self.view.window!.rootViewController?.dismiss(animated: true, completion: nil)
        performSegue(withIdentifier: "ResultChatToSubtopic", sender: sender)
    }
    
    
}
