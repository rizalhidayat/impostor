//
//  IntroObrolanVC.swift
//  Locla
//
//  Created by Bagus setiawan on 13/08/20.
//  Copyright © 2020 Apple Developer Academy. All rights reserved.
//

import UIKit

class IntroObrolanVC: UIViewController {
    
    @IBOutlet weak var judulSubtopikLabel: UILabel!
    @IBOutlet weak var subtopikImageView: UIImageView!
    @IBOutlet weak var deskripsiLabel: UILabel!
    @IBOutlet weak var misiLabel: UILabel!
    @IBOutlet weak var mulaiButton: UIButton!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var viewMisi: UIView!
    @IBOutlet weak var viewLbMisi: UIView!
    
    
    var subtopik : Subtopic?
    var subtopicId: Int?
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        configureButton()
        configureLabel()
        configureImage()
        configureView()
    }
    
    func configureButton(){
        self.closeButton.layer.cornerRadius = 16.0
        self.closeButton.layer.borderWidth = 0.1
        self.closeButton.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        
        self.mulaiButton.layer.cornerRadius = 26.5
        self.mulaiButton.layer.borderWidth = 1.0
        self.mulaiButton.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    }
    
    func configureLabel(){
        self.judulSubtopikLabel.text = subtopik?.name
        //self.deskripsiLabel.text = subtopik?.mission
        self.misiLabel.text = subtopik?.subtopicDescription
    }
    
    func configureImage(){
        self.subtopikImageView.image = UIImage(named: subtopik?.imageFilename ?? "home")
    }
    
    func configureView(){
        self.viewMisi.layer.cornerRadius = 12.0
        self.viewMisi.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        self.viewMisi.layer.borderWidth = 2.0
        self.viewMisi.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        
        self.viewLbMisi.layer.cornerRadius = 12.0
        self.viewLbMisi.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
        self.viewLbMisi.layer.borderWidth = 2.0
        self.viewLbMisi.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        
        //Custom border
        //addCustomBorders()
    }
    
    @IBAction func didCloseBtn(_ sender: Any) {
        let alertVC = UIStoryboard(name: "CustomAlert", bundle: nil).instantiateViewController(withIdentifier: "AlertExitVC") as! AlertExitVC
        
        alertVC.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        self.addChild(alertVC)
        self.view.addSubview(alertVC.view)
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "IntroToChallengeChat" {
            guard let challengeChatVC = segue.destination as? ChallengeObrolanVC
                else {
                    return
            }
            challengeChatVC.subtopicID = self.subtopik?.id
        }
    }
}
