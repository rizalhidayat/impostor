//
//  SearchBarVC.swift
//  Locla
//
//  Created by Bagus setiawan on 12/08/20.
//  Copyright © 2020 Apple Developer Academy. All rights reserved.
//

import UIKit
import AVFoundation

class SearchBarVC: UIViewController {
    
    var categoriID : Int?
    var searchBarVocabulary : [Vocabulary] = []
    var dataResult : [Vocabulary] = []
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureCollectionView()
    }
    
    func configureCollectionView(){
        registerCollectionView()
    }
    
    func registerCollectionView(){
        let nib = UINib(nibName: ListKosakataCollectionViewCell.cellID, bundle: Bundle.main)
        self.collectionView.register(nib, forCellWithReuseIdentifier: ListKosakataCollectionViewCell.cellID)
    }
    
    func playSound(for index: Int){
        let generator = UINotificationFeedbackGenerator()
        generator.notificationOccurred(.success)
        Petunjuk.playPetunjuk(petunjukName: self.dataResult[index].soundFilename ?? "")
    }
}

extension SearchBarVC: UICollectionViewDelegate, UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataResult.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ListKosakataCollectionViewCell", for: indexPath) as! ListKosakataCollectionViewCell
        cell.kosakataLabel.text = self.dataResult[indexPath.row].meaning ?? ""
        cell.terjemahanLabel.text = self.dataResult[indexPath.row].word ?? ""
        cell.indexPath = indexPath
        cell.delegate = self
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        //playSound(for: indexPath.row)
        let alertDetailVC = UIStoryboard(name: "CustomAlert", bundle: nil).instantiateViewController(withIdentifier: "AlertVocabDetailVC") as! AlertVocabDetailVC
        alertDetailVC.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        alertDetailVC.indexPath = indexPath
        alertDetailVC.kosakataLabel.text = self.dataResult[indexPath.row].meaning ?? ""
        alertDetailVC.indonesiaLabel.text = self.dataResult[indexPath.row].word ?? ""
        alertDetailVC.kalimatDaerah.text = dataResult[indexPath.row].exampleLocal ?? ""
        alertDetailVC.kalimatIndonesia.text = dataResult[indexPath.row].exampleIndo ?? ""
        alertDetailVC.delegate = self
        
        self.addChild(alertDetailVC)
        alertDetailVC.view.frame = view.bounds
        self.view.addSubview(alertDetailVC.view)
        collectionView.isScrollEnabled = false
    }
}

extension SearchBarVC : UISearchControllerDelegate{
    
    func search(_ searchBar: UISearchBar, textDidChange searchText: String){
        if searchText.isEmpty {
            searchBarVocabulary = dataResult
        }else {
            searchBarVocabulary = dataResult.filter{searchBarVocabulary in (searchBarVocabulary.word?.lowercased().contains(searchText.lowercased()))! || ((searchBarVocabulary.meaning?.lowercased().contains(searchText.lowercased()))!)
            }
        }
        dataResult = searchBarVocabulary
        collectionView.reloadData()
    }
}

extension SearchBarVC : ListKosakataCollectionViewCellDelegate{
    func clickedSoundButton(for index: Int) {
        playSound(for: index)
    }
}

extension SearchBarVC : AlertVocabDetailVCDelegate{
    func clickedClose() {
        collectionView.isScrollEnabled = true
    }
    
    func clickedSpeaker(for index: Int) {
        playSound(for: index)
    }
}
