//
//  AlertVocabDetailVC.swift
//  Dialokal
//
//  Created by Tony Varian Yoditanto on 25/10/20.
//  Copyright © 2020 Apple Developer Academy. All rights reserved.
//

import UIKit

protocol AlertVocabDetailVCDelegate {
    func clickedSpeaker(for index : Int)
    func clickedClose()
}

class AlertVocabDetailVC: UIViewController {

    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var kosakataLabel: UILabel!
    @IBOutlet weak var speakerButton: UIButton!
    @IBOutlet weak var keterangan1Label: UILabel!
    @IBOutlet weak var keterangan2Label: UILabel!
    @IBOutlet weak var kalimatDaerah: UILabel!
    @IBOutlet weak var kalimatIndonesia: UILabel!
    @IBOutlet weak var customAlertView: UIView!
    @IBOutlet weak var viewIndonesia: UIView!
    @IBOutlet weak var indonesiaLabel: UILabel!
    var indexPath : IndexPath?
    var delegate : AlertVocabDetailVCDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        configureButton()
        // Do any additional setup after loading the view.
    }
    
    func configureView(){
        self.customAlertView.layer.cornerRadius = 21.0
        self.customAlertView.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.customAlertView.layer.borderWidth = 2.0
    }
    
    func configureButton(){
        self.speakerButton.layer.cornerRadius = 18.0
        self.speakerButton.layer.borderWidth = 2.0
        self.speakerButton.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        
        self.closeButton.layer.cornerRadius = 16.0
        self.closeButton.layer.borderWidth = 2.0
        self.closeButton.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        
        //self.indonesiaLabel.setTitle(, for: .disabled)
        self.viewIndonesia.layer.cornerRadius = 20.0
        self.viewIndonesia.layer.borderWidth = 2.0
        self.viewIndonesia.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    }
    
    @IBAction func didTapCloseButton(_ sender: Any) {
        self.delegate?.clickedClose()
        self.removeFromParent()
        self.view.removeFromSuperview()
    }
    
    @IBAction func didTapSpeakerButton(_ sender: Any) {
        self.delegate?.clickedSpeaker(for: indexPath?.row ?? 0)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
