//
//  VocabularyDetailVC.swift
//  Locla
//
//  Created by Bagus setiawan on 10/08/20.
//  Copyright © 2020 Apple Developer Academy. All rights reserved.
//

import UIKit
import AVFoundation

class VocabularyDetailVC: UIViewController {
    
    
    @IBOutlet var collectionView: UICollectionView!
    
    var categoriID : Int?
    var vocabularies : [Vocabulary] = []
    var meaningSound : AVAudioPlayer?
    var sound : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureCollectionView()
        fetchVocabularies()
        
    }
    
    func configureCollectionView(){
        registerCollectionView()
    }
    
    func registerCollectionView(){
        let nib = UINib(nibName: ListKosakataCollectionViewCell.cellID, bundle: Bundle.main)
        self.collectionView.register(nib, forCellWithReuseIdentifier: ListKosakataCollectionViewCell.cellID)
    }
    
    func fetchVocabularies(){
        vocabularies = DataLoader.getVocabularies(categoryID: categoriID ?? 1)
    }
}

extension VocabularyDetailVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return vocabularies.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ListKosakataCollectionViewCell.cellID, for: indexPath) as! ListKosakataCollectionViewCell
        cell.kosakataLabel.text = vocabularies[indexPath.row].meaning ?? ""
        cell.terjemahanLabel.text = vocabularies[indexPath.row].word ?? ""
        cell.indexPath = indexPath
        cell.delegate = self
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        //playSound(for: indexPath.row)
        let alertDetailVC = UIStoryboard(name: "CustomAlert", bundle: nil).instantiateViewController(withIdentifier: "AlertVocabDetailVC") as! AlertVocabDetailVC
        alertDetailVC.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        alertDetailVC.indexPath = indexPath
        alertDetailVC.kosakataLabel.text = vocabularies[indexPath.row].meaning ?? ""
        alertDetailVC.indonesiaLabel.text = vocabularies[indexPath.row].word ?? ""
        alertDetailVC.kalimatDaerah.text = vocabularies[indexPath.row].exampleLocal ?? ""
        alertDetailVC.kalimatIndonesia.text = vocabularies[indexPath.row].exampleIndo ?? ""
        alertDetailVC.delegate = self
        
        
        self.addChild(alertDetailVC)
        alertDetailVC.view.frame = view.bounds
        self.view.addSubview(alertDetailVC.view)
        collectionView.isScrollEnabled = false
        

    }
    
    func playSound(for index: Int){
        let generator = UINotificationFeedbackGenerator()
        generator.notificationOccurred(.success)
        Petunjuk.playPetunjuk(petunjukName: self.vocabularies[index].soundFilename ?? "")
    }
     
}

extension VocabularyDetailVC : ListKosakataCollectionViewCellDelegate{
    func clickedSoundButton(for index: Int) {
        playSound(for: index)
    }
}

extension VocabularyDetailVC : AlertVocabDetailVCDelegate{
    
    func clickedSpeaker(for index: Int) {
        playSound(for: index)
    }
    
    func clickedClose(){
        collectionView.isScrollEnabled = true
    }
}
