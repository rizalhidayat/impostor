//
//  ChallengeListeningVC.swift
//  Locla
//
//  Created by Tony Varian Yoditanto on 12/08/20.
//  Copyright © 2020 Apple Developer Academy. All rights reserved.
//

import UIKit

class ChallengeListeningVC: UIViewController {
    
    var colors : [UIColor] = [#colorLiteral(red: 0.968627451, green: 0.7843137255, blue: 0.262745098, alpha: 1),#colorLiteral(red: 0, green: 0.768627451, blue: 0.7254901961, alpha: 1),#colorLiteral(red: 0.6941176471, green: 0, blue: 0, alpha: 1),#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1),#colorLiteral(red: 0.5568627451, green: 0.7490196078, blue: 0.4039215686, alpha: 1),#colorLiteral(red: 0.4901960784, green: 0.1411764706, blue: 0.2274509804, alpha: 1)]
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var progressViewLabel: UILabel!
    @IBOutlet weak var pertanyaanLabel: UILabel!
    @IBOutlet weak var petunjukButton: UIButton!
    @IBOutlet weak var keterangan1Label: UILabel!
    @IBOutlet weak var keterangan2Label: UILabel!
    @IBOutlet weak var buttonA: UIButton!
    @IBOutlet weak var buttonB: UIButton!
    @IBOutlet weak var buttonC: UIButton!
    @IBOutlet weak var buttonD: UIButton!
    @IBOutlet weak var viewInputUser: UIView!
    @IBOutlet weak var closeButton: UIButton!
    
    var subtopik : Subtopic!
    var challenge = [ListeningChallenge]()
    var challenge2 = [Subtopic]()
    var totalQuestion : Int = 0
    var currentQuestion : Int = 1
    var answer = ""
    var pertanyaan = ""
    var choiceA = ""
    var choiceB = ""
    var choiceC = ""
    var choiceD = ""
    //var player : AVAudioPlayer?
    var petunjuk : String = ""
    var starGained = 0
    var currentStar = 3
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fetchChallenge()
        configureButton()
        configureLabel()
        configureAnswer()
        configureProgressView()
        configureViewInputUser()
        // Do any additional setup after loading the view.
    }
    
    func fetchChallenge(){
        //self.challenge = DataLoader.getVocabularyChallenges(subtopicID: subtopik.id)
        self.challenge = DataLoader.getListeningChallenges(subtopicID: subtopik.id)
        self.totalQuestion = challenge.count
        self.answer = self.challenge[currentQuestion-1].answer ?? ""
        self.pertanyaan = self.challenge[currentQuestion-1].question ?? ""
        self.choiceA = self.challenge[currentQuestion-1].optionA ?? ""
        self.choiceB = self.challenge[currentQuestion-1].optionB ?? ""
        self.choiceC = self.challenge[currentQuestion-1].optionC ?? ""
        self.choiceD = self.challenge[currentQuestion-1].optionD ?? ""
        
    }
    
    func configureButton(){
        
        self.buttonA.layer.cornerRadius = 24.0
        self.buttonB.layer.cornerRadius = 24.0
        self.buttonC.layer.cornerRadius = 24.0
        self.buttonD.layer.cornerRadius = 24.0
        
        self.buttonA.backgroundColor = colors[0]
        self.buttonB.backgroundColor = colors[0]
        self.buttonC.backgroundColor = colors[0]
        self.buttonD.backgroundColor = colors[0]
        
        self.buttonA.setTitleColor(colors[3], for: .normal)
        self.buttonB.setTitleColor(colors[3], for: .normal)
        self.buttonC.setTitleColor(colors[3], for: .normal)
        self.buttonD.setTitleColor(colors[3], for: .normal)
        
        self.buttonA.layer.borderWidth = 2.0
        self.buttonB.layer.borderWidth = 2.0
        self.buttonC.layer.borderWidth = 2.0
        self.buttonD.layer.borderWidth = 2.0
        
        self.buttonA.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.buttonB.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.buttonC.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.buttonD.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        
        self.closeButton.layer.cornerRadius = 16.0
        self.closeButton.layer.borderWidth = 0.1
        self.closeButton.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    }
    
    func configureLabel(){
        self.pertanyaanLabel.text = self.pertanyaan
        self.keterangan1Label.isHidden = true
        self.keterangan2Label.isHidden = true
    }
    
    func configureAnswer(){
        self.buttonA.setTitle(self.choiceA, for: .normal)
        self.buttonB.setTitle(self.choiceB, for: .normal)
        self.buttonC.setTitle(self.choiceC, for: .normal)
        self.buttonD.setTitle(self.choiceD, for: .normal)
    }
    
    func configureProgressView(){
        self.progressView.layer.cornerRadius = 10.0
        self.progressView.layer.masksToBounds = true
        self.progressViewLabel.text = "\(currentQuestion)/\(totalQuestion)"
        self.progressView.progress = Float(currentQuestion) / Float(totalQuestion)
        self.progressView.layer.borderWidth = 2.0
        self.progressView.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    }
    
    func configureViewInputUser(){
        self.viewInputUser.layer.cornerRadius = 21.0
        self.viewInputUser.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.viewInputUser.layer.borderWidth = 2.0
    }
    
    func resetColorButton(){
        if buttonA.currentTitleColor == colors[0] || buttonB.currentTitleColor == colors[0] || buttonC.currentTitleColor == colors[0] || buttonD.currentTitleColor == colors[0]{
            self.buttonA.setTitleColor(colors[3], for: .normal)
            self.buttonB.setTitleColor(colors[3], for: .normal)
            self.buttonC.setTitleColor(colors[3], for: .normal)
            self.buttonD.setTitleColor(colors[3], for: .normal)
        }
    }
    
    @IBAction func didTapButtonA(_ sender: Any) {
        self.buttonA.backgroundColor = colors[1]
        resetColorButton()
        if "A" == answer{
            self.buttonA.backgroundColor = colors[0]
            self.buttonB.backgroundColor = colors[0]
            self.buttonC.backgroundColor = colors[0]
            self.buttonD.backgroundColor = colors[0]
            rightAnswer()
        }
        
        if "A" != answer{
            self.buttonA.backgroundColor = colors[2]
            self.buttonA.setTitleColor(colors[0], for: .normal)
            self.buttonB.backgroundColor = colors[0]
            self.buttonC.backgroundColor = colors[0]
            self.buttonD.backgroundColor = colors[0]
            wrongAnswer()
        }
    }
    
    
    @IBAction func didTapButtonB(_ sender: Any) {
        self.buttonB.backgroundColor = colors[1]
        resetColorButton()
        if "B" == answer{
            self.buttonB.backgroundColor = colors[0]
            self.buttonA.backgroundColor = colors[0]
            self.buttonC.backgroundColor = colors[0]
            self.buttonD.backgroundColor = colors[0]
            rightAnswer()
        }
        
        if "B" != answer{
            self.buttonA.backgroundColor = colors[0]
            self.buttonB.backgroundColor = colors[2]
            self.buttonB.setTitleColor(colors[0], for: .normal)
            self.buttonC.backgroundColor = colors[0]
            self.buttonD.backgroundColor = colors[0]
            wrongAnswer()
        }
    }
    
    @IBAction func didTapButtonC(_ sender: Any) {
        self.buttonC.backgroundColor = colors[1]
        resetColorButton()
        if "C" == answer{
            self.buttonC.backgroundColor = colors[0]
            self.buttonA.backgroundColor = colors[0]
            self.buttonB.backgroundColor = colors[0]
            self.buttonD.backgroundColor = colors[0]
            rightAnswer()
        }
        
        if "C" != answer{
            self.buttonA.backgroundColor = colors[0]
            self.buttonB.backgroundColor = colors[0]
            self.buttonC.backgroundColor = colors[2]
            self.buttonC.setTitleColor(colors[0], for: .normal)
            self.buttonD.backgroundColor = colors[0]
            wrongAnswer()
        }
    }
    
    @IBAction func didTapButtonD(_ sender: Any) {
        self.buttonD.backgroundColor = colors[1]
        resetColorButton()
        if "D" == answer{
            self.buttonD.backgroundColor = colors[0]
            self.buttonA.backgroundColor = colors[0]
            self.buttonB.backgroundColor = colors[0]
            self.buttonC.backgroundColor = colors[0]
            rightAnswer()
        }
        
        if "D" != answer{
            self.buttonA.backgroundColor = colors[0]
            self.buttonB.backgroundColor = colors[0]
            self.buttonC.backgroundColor = colors[0]
            self.buttonD.backgroundColor = colors[2]
            self.buttonD.setTitleColor(colors[0], for: .normal)
            wrongAnswer()
        }
    }
    
    func rightAnswer(){
        self.currentQuestion += 1
        let alertVC = UIStoryboard(name: "CustomAlert", bundle: nil).instantiateViewController(withIdentifier: "AlertStarVC") as! AlertStarVC
        alertVC.starCount = getTotalStar()
        alertVC.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        alertVC.delegateFromVocab = self
        
        alertVC.parentID = 1
        self.addChild(alertVC)
        self.view.addSubview(alertVC.view)
    }
    
    func wrongAnswer(){
        self.keterangan1Label.isHidden = false
        self.keterangan2Label.isHidden = false
        removeStar()
        let generator = UINotificationFeedbackGenerator()
        generator.notificationOccurred(.error)
    }
    
    func removeStar(){
        if currentStar > 1 {
            self.currentStar -= 1
        }
    }
    
    @IBAction func didTapCloseButton(_ sender: Any) {
        let alertVC = UIStoryboard(name: "CustomAlert", bundle: nil).instantiateViewController(withIdentifier: "AlertExitVC") as! AlertExitVC
        alertVC.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        self.addChild(alertVC)
        self.view.addSubview(alertVC.view)
    }
    
    func getTotalStar()->Int{
        self.starGained += currentStar
        return currentStar
    }
    
    func configureChallengeState(){
        currentStar = 3
    }
    
}

extension ChallengeListeningVC : AlertStarVCDelegate{
    
    func reload(){
        if currentQuestion <= totalQuestion {
            fetchChallenge()
            configureChallengeState()
            configureButton()
            configureLabel()
            configureAnswer()
            configureProgressView()
            configureViewInputUser()
        }
        
        if currentQuestion > totalQuestion {
            let storyboard = UIStoryboard(name: "Result", bundle: nil)
            let destination = storyboard.instantiateViewController(identifier: "ResultVC") as! ResultVC
            destination.modalPresentationStyle = .fullScreen
            destination.modalTransitionStyle = .crossDissolve
            destination.subtopik = self.subtopik
            destination.starGained = self.starGained
            destination.totalQuestion = self.totalQuestion
            self.present(destination, animated: true, completion: nil)
        }
    }
}
