//
//  IntroListeningVC.swift
//  Locla
//
//  Created by Tony Varian Yoditanto on 12/08/20.
//  Copyright © 2020 Apple Developer Academy. All rights reserved.
//

import UIKit

class IntroListeningVC: UIViewController {

    @IBOutlet weak var judulSubtopikLabel: UILabel!
    @IBOutlet weak var subtopikImageView: UIImageView!
    @IBOutlet weak var deskripsiLabel: UILabel!
    @IBOutlet weak var misiLabel: UILabel!
    @IBOutlet weak var mulaiButton: UIButton!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var viewLbMisi: UIView!
    @IBOutlet weak var viewMisi: UIView!
    var subtopik : Subtopic?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.subtopik = DataLoader.getSubtopic(subtopicID: <#T##Int#>)
        configureButton()
        configureImage()
        configureLabel()
        configureView()
    }
        
    func configureButton(){
        self.closeButton.layer.cornerRadius = 16.0
        self.closeButton.layer.borderWidth = 0.1
        self.closeButton.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        
        self.mulaiButton.layer.cornerRadius = 26.5
        self.mulaiButton.layer.borderWidth = 1.0
        self.mulaiButton.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    }
        
    func configureLabel(){
        self.judulSubtopikLabel.text = subtopik?.name ?? ""
        self.misiLabel.text = subtopik?.subtopicDescription ?? ""
    }
        
    func configureImage(){
        subtopikImageView.image = UIImage(named: self.subtopik?.imageFilename ?? "home")
    }
    
    func configureView(){
        self.viewMisi.layer.cornerRadius = 12.0
        self.viewMisi.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        self.viewMisi.layer.borderWidth = 2.0
        self.viewMisi.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        
        self.viewLbMisi.layer.cornerRadius = 12.0
        self.viewLbMisi.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
        self.viewLbMisi.layer.borderWidth = 2.0
        self.viewLbMisi.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "IntroToDialogListening" {
            guard let DialogListeningVC = segue.destination as? DialogListeningVC
                else {
                    return
            }
            DialogListeningVC.subtopik = subtopik
        }
    }
    
    @IBAction func didTapCloseButton(_ sender: Any) {
        let alertVC = UIStoryboard(name: "CustomAlert", bundle: nil).instantiateViewController(withIdentifier: "AlertExitVC") as! AlertExitVC
        
        alertVC.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        self.addChild(alertVC)
        self.view.addSubview(alertVC.view)
    }
    

}
