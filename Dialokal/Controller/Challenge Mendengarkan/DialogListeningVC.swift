//
//  DialogListeningVC.swift
//  Locla
//
//  Created by Tony Varian Yoditanto on 12/08/20.
//  Copyright © 2020 Apple Developer Academy. All rights reserved.
//

import UIKit
import AVFoundation

class DialogListeningVC: UIViewController {
    
    var subtopik : Subtopic!
    var dialogFileName = ""
    var listeningChallenge = [ListeningChallenge]()
    var player : AVAudioPlayer?
    
    @IBOutlet weak var judulSubtopikLabel: UILabel!
    @IBOutlet weak var subtopikImageView: UIImageView!
    @IBOutlet weak var hintButton: UIButton!
    @IBOutlet weak var mulaiButton: UIButton!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var misiLabel: UILabel!
    @IBOutlet weak var viewMisi: UIView!
    @IBOutlet weak var viewLbMisi: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureButton()
        configureImage()
        configureLabel()
        configureView()
        fetchChallenge()
        // Do any additional setup after loading the view.
    }
    
    func fetchChallenge(){
        
    }
    
    func configureButton(){
        self.closeButton.layer.cornerRadius = 16.0
        self.closeButton.layer.borderWidth = 0.1
        self.closeButton.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        
        self.mulaiButton.layer.cornerRadius = 26.5
        self.mulaiButton.layer.borderWidth = 1.0
        self.mulaiButton.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        
        self.hintButton.layer.cornerRadius = 35.0
        self.hintButton.layer.borderWidth = 2.0
        self.hintButton.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        
        self.mulaiButton.isEnabled = false
        self.mulaiButton.backgroundColor = UIColor(named: "InactiveYellow")
    }
    
    func configureLabel(){
        judulSubtopikLabel.text = subtopik?.name ?? ""
        misiLabel.text = "Pastikan untuk menyimak audio berikut sebelum memulai tantangan. Kamu tidak bisa mendengarkan audio lagi ketika kamu sudah mulai mendengarkan"
    }
    
    func configureImage(){
        subtopikImageView.image = UIImage(named: self.subtopik?.imageFilename ?? "Home")
    }
    
    func configureView(){
        //self.viewMisi.isHidden = true
    }
    
    @IBAction func didTapDialogButton(_ sender: Any) {
        
        self.dialogFileName = self.subtopik.listeningFilename ?? ""
        let dialogURL = Bundle.main.url(forResource: self.dialogFileName, withExtension: "mp3")
        
        do {
            try player = AVAudioPlayer(contentsOf: dialogURL!)
        } catch {
            print(error)
        }
        
        if !(player?.isPlaying ?? false){
            player?.play()
        }
        self.mulaiButton.isEnabled = true
        self.mulaiButton.backgroundColor = UIColor(named: "PrimaryYellow")
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "DialogToChallengeListening" {
            guard let ChallengeListeningVC = segue.destination as? ChallengeListeningVC
                else {
                    return
            }
            ChallengeListeningVC.subtopik = self.subtopik
        }
    }
    
    @IBAction func didTapMulaiButton(_ sender: Any) {
        self.player?.stop()
    }
    
    @IBAction func didTapCloseButton(_ sender: Any) {
        let alertVC = UIStoryboard(name: "CustomAlert", bundle: nil).instantiateViewController(withIdentifier: "AlertExitVC") as! AlertExitVC
        
        alertVC.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        self.addChild(alertVC)
        self.view.addSubview(alertVC.view)
    }
}
