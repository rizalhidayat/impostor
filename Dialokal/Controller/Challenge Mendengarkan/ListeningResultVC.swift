//
//  ListeningResultVC.swift
//  Locla
//
//  Created by Tony Varian Yoditanto on 12/08/20.
//  Copyright © 2020 Apple Developer Academy. All rights reserved.
//

import UIKit

class ListeningResultVC: UIViewController {

    @IBOutlet weak var subtopikImageview: UIImageView!
    @IBOutlet weak var resultIndoLabel: UILabel!
    @IBOutlet weak var resultLocalLabel: UILabel!
    @IBOutlet weak var star1Imageview: UIImageView!
    @IBOutlet weak var star2Imageview: UIImageView!
    @IBOutlet weak var star3Imageview: UIImageView!
    @IBOutlet weak var jempolImageview: UIImageView!
    @IBOutlet weak var percobaanPertamaLabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var randomTipLabel: UILabel!
    @IBOutlet weak var selesaiButton: UIButton!
    @IBOutlet weak var viewRandomTip: UIView!
    @IBOutlet weak var totalBintangImageview: UIImageView!
    @IBOutlet weak var totalBintangLabel: UILabel!
    
    
    
    var subtopik : Subtopic!
    var vocabularies : [Vocabulary] = []
    var starGained = 0
    var firstTry = 0
    var totalQuestion = 0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        saveStarAndFirstTry()
        configureButton()
        configureLabel()
        configureImage()
        fetchListKosakata()
    }

    func fetchListKosakata(){
        self.vocabularies = DataLoader.getVocabularies(subtopicID: subtopik?.id ?? 1)
    }
    
    func saveStarAndFirstTry(){
        DataLoader.updateSubtopic(subtopicId: self.subtopik.id, star: self.starGained, firstTry: firstTry)
        
    }
    
    func configureButton(){
        self.selesaiButton.layer.cornerRadius = 10.0

    }
    
    func configureLabel(){
        self.resultIndoLabel.text = "Kamu yang terbaik!!"
        self.resultLocalLabel.text = "Kon sangar"
        self.randomTipLabel.text = "*Random Tip : Cok biasanya di gunakan kalau kondisi sudah akrab, jangan pada orang yang baru anda kenal"
        self.viewRandomTip.layer.cornerRadius = 10.0
        self.percobaanPertamaLabel.text = "\(self.firstTry)/\(self.totalQuestion)"
        self.totalBintangLabel.text = "\(self.starGained)/\(self.totalQuestion*3)"
        let locationID : Int = DataLoader.getCurrentLocation()
        self.randomTipLabel.text = DataLoader.getRandomTip(locationID: locationID)
        self.resultLocalLabel.isHidden = true

    }
    
    func configureImage(){
        self.subtopikImageview.image = UIImage(named: "people1")
        self.star1Imageview.image = UIImage(named: "Star")
        self.star2Imageview.image = UIImage(named: "Star")
        self.star3Imageview.image = UIImage(named: "Star")
        self.jempolImageview.image = UIImage(named: "Image-1")
        self.totalBintangImageview.image = UIImage(named: "Star")
        self.star1Imageview.isHidden = true
        self.star2Imageview.isHidden = true
        self.star3Imageview.isHidden = true

    }
    
    @IBAction func didTapSelesaiButton(_ sender: Any) {
//        self.view.window!.rootViewController?.dismiss(animated: true, completion: nil)
        performSegue(withIdentifier: "ResultListeningToSubtopic", sender: sender)
    }
}
